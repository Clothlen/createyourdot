# Battles

"Battle" refers to any encounter with anything, where an enemy appears to
fight the player. This is controlled by the Battle scene, located at
`res://CreateYourDot/Scenes/Battle/Battle.tscn`.

Any mod can define an *encounter*. The battle scene can only run one
encounter at a time.

Mods can extend the Encounter class, located at
`res://CreateYourDot/Encounters/Encounter.cs`. After the Battle scene
has loaded itself, it will invoke its encounter's `Create` method.

Afterwards, the `Update` method is invoked every physics tick, regardless
of what state the game is in.

Mods *register* encounters inside of their Mod class constructor.

Enemies are intended to be created within the `Create` method.
Use the `AddEnemy` method. Locations are used to refer to enemies.

For example, the Encounter Skeleton mod's Create method looks like
this:

```cs
public override void Create() {
    this.AddEnemy("EncounterSkeleton:Poseur");
}
```

Note the structure of the location (string). It is a combination of
the mod's internal id and the id of the enemy. The former is defined
in the mod class, and the latter is defined in the enemy class.

## Enemy Classes

Enemy classes are always attached to a Godot scene. Godot scenes are
.tscn files, and are best edited with the Godot editor. The scene
must have a C# script attached.

In addition, the script *must* extend `CreateYourDot
