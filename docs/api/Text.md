# Text

Create Your Dot implements some of the features of Create Your Frisk's text features.

## Implemented Features

### Color

`[color:ffffff]`

Enter any hexadecimal color code. All following text will have that color.

You can also control alpha by *prepending* an additional two characters:

`[color:ffffffff]` (opaque and white)

`[color:00000000]` (transparent and black)

`[color:00ffffff]` (transparent and white)

`[color:55ffffff]` (partially transparent and white)

`[color:ffff55]` (fully opaque and yellow)

The alpha defaults to `ff` if not set. `ff` means opaque, and `00` means
transparent; it's hexadecimal.

Additional color tags will override the alpha as well, implicitly.

### Wait

`[wait:1]` `[wait:0.2]`

Wait for a number of seconds. Text will not advance until the time has passed.

This accepts floats. This does not accept negative values.

Note that this waits *seconds*, unlike CYF. In CYF, it would wait for `n * 4`
*frames*. This runs at Godot `ProjectSettings.Physics.Physics Fps` (60), so to simulate
CYF wait times, use `n * 4 / 60` (I think).

(It may be better to have a `waitsec` command for seconds, `waitframe` command for frames, and a
`wait` command that acts identically to CYF's `wait` command.)

### Speed

`[speed:0.25]`

Set the speed that text advances. The default is defined in `CreateYourDot.Scenes.Text.TextBox.DEFAULT_SPEED`.

### Instant

`[instant]` `[instant:stop]` `[instant:start]`

Instantly finish the text. It will advance to the end immediately. Wait commands are ignored, but all other
commands still work (even speed commands), but they may have no effect (as with speed commands).

If `[instant:stop]` is encountered, the text box behavior will return to normal.

### Line Spacing

`[linespacing:10]` `[linespacing:default]`

Set the line spacing. This will only have an effect on lines going forward.

### Character Spacing

`[charspacing:9]` `[charspacing:default]`

Set the space between characters in pixels.

### Font

Set the font. Currently only works with the built-in fonts found in `resources/fonts`.

Some fonts do not work with some text.

## Custom Features

### Bracket

`[bracket:left]` `[bracket:right]`

This allows you to display commands as text without running them. This command replaces itself
with a bracket; either a left or right one.

For example:

`[bracket:left]color:ff0000[bracket:right]`

will display:

`[color:ff0000]`

without changing the color.

## Unimplemented Features

The following tags are not implemented:

* `starcolor`
* `effect`
* `lettereffect`
* `noskip`
* `voice`
* `next`
* `finished`
* `nextthisnow`
* `waitfor`
* `func`
* `letters`
* `name`
  * Just grab the player's name yourself!
* `music`
* `sound`
* `mugshot`
* `health`
  * Considering there could be multiple players, this will likely
never be implemented.
* `skipover`
* `skiponly`
