namespace EncounterSkeleton.Projectiles {
	public class ChaserOrb : CreateYourDot.Projectiles.ProjectileController
	{

		private float _xspeed = 0;

		private float _yspeed = 0;

		public override void Create() {
			base.Create();
			this.CreateAsSprite("res://mods/EncounterSkeleton/resources/bullet.png");
		}

		public override void Update(float delta) {
			base.Update(delta);

			var xdifference = this.Battle.Player.RPosition.x - this.Position.x;
			var ydifference = this.Battle.Player.RPosition.y - this.Position.y;
			var xspeed = this._xspeed / 2 + xdifference / 100;
			var yspeed = this._yspeed / 2 + ydifference / 100;
			this.Move(xspeed, yspeed);
			this._xspeed = xspeed;
			this._yspeed = yspeed;

		}

		public override void OnHit(CreateYourDot.Projectiles.IDamageable target) {
			target.Damage(3.0f);
		}

	}
}
