namespace EncounterSkeleton.Projectiles {
	using System;
	using Godot;

	public class TouhouBullet : CreateYourDot.Projectiles.ProjectileController
	{

		public float Mult;

		public float Offset;

		public float YOffset;

		private float _timer = 0;

		private float _lerp = 0;

		public override void Create() {
			base.Create();

			this.CreateAsSprite("res://mods/EncounterSkeleton/resources/bullet.png");
		}

		public override void Update(float delta) {
			base.Update(delta);

			var posx = (70 * this._lerp) * MathF.Sin(this._timer * this.Mult + this.Offset);
			var posy = (70 * this._lerp) * MathF.Cos(this._timer + this.Offset) + this.YOffset - this._lerp * 50;
			this.MoveTo(posx, posy);
			this._timer += 1.0f / 40;
			this._lerp += 1.0f / 90;
			if (this._lerp > 4.0f) {
				this._lerp = 4.0f;
			}

		}
	}
}
