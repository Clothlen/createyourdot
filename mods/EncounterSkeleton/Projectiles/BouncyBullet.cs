namespace EncounterSkeleton.Projectiles {
	using Godot;

	public class BouncyBullet : CreateYourDot.Projectiles.ProjectileController
	{

		// Height of this bullet.
		// Could get it automatically from collision body,
		// but eh.
		private const int _HEIGHT = 8;

		public float velx;
		public float vely;

		public override void Create() {
			base.Create();

			this.CreateAsSprite("res://mods/EncounterSkeleton/resources/bullet.png");
		}

		public override void Update(float delta) {
			base.Update(delta);

			var newposx = this.x + this.velx;
			var newposy = this.y + this.vely;
			if (this.x > -this.Arena.Dimensions.x / 2 && this.x < this.Arena.Dimensions.x / 2) {
				if (this.y < -this.Arena.Dimensions.y / 2 + _HEIGHT) {
					newposy = -this.Arena.Dimensions.y / 2 + _HEIGHT;
					this.vely = 4;
				}
			}

			this.vely -= 0.04f;

			this.MoveTo(newposx, newposy);

		}

		public override void OnHit(CreateYourDot.Projectiles.IDamageable target) {
			target.Damage(3);
		}

	}
}
