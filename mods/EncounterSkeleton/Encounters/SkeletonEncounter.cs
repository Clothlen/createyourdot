namespace EncounterSkeleton.Encounters {
	public sealed class SkeletonEncounter : CreateYourDot.Encounters.Encounter
	{

		public override string Id => "SkeletonEncounter";

		public override void Create() {
			this.AddEnemy<Enemies.Poseur>();

		}

		public override void Update(float delta) {
		}

	}
}
