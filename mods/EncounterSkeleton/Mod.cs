namespace EncounterSkeleton {
	public class Mod : CreateYourDot.Modding.Mod
	{

		public override string Id => "EncounterSkeleton";

		public override void RegisterEncounters(CreateYourDot.Modding.Registers.EncounterRegister register) {
			register.Register<Encounters.SkeletonEncounter>();
		}

		// public override void Battle(CreateYourDot.Scenes.Battle.Battle battle) {
		// 	// battle.AddChild(Godot.ResourceLoader.Load<Godot.PackedScene>("res://mods/EncounterSkeleton/Monsters/Poseur.tscn").Instance());
		// }
	}
}
