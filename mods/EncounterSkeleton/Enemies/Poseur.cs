namespace EncounterSkeleton.Enemies {
	public class Poseur : CreateYourDot.Enemies.Enemy
	{

		public override string Id => "Poseur";

		public override string Name { get; set; } = "Poseur";

		public override float MaxHealth { get; set; } = 100;

		public override float Attack { get; set; } = 1;

		public override float Defense { get; set; } = 1;

		public override string CheckText { get; set; } = "Check message goes here.";

		public override string[] Comments { get; set; } = new[] {
			"Smells like the work\rof an enemy stand.",
			"Poseur is posing like his\rlife depends on it.",
			"Poseur's limbs shouldn't be\rmoving in this way."
		};

		public override string[] Commands { get; set; } = new[] {
			"Act 1",
			"Act 2",
			"Act 3"
		};

		public override string[] Dialogue { get; set; } = new[] {
			"Random\nDialogue\n1.",
			"Random\nDialogue\n2.",
			"Random\nDialogue\n3."
		};

		public override void Update(float delta) { }

		public override void OnAttack() {
			Godot.GD.Print("Starting attack by Poseur");
			this.StartRandomWave(new[] {
				typeof(Waves.TouhouWave),
				typeof(Waves.ChaserOrbWave),
				typeof(Waves.BouncyWave)
			});
		}

		public override void OnCommand(string command) {
			if (command == "Act 1") {
				this.Dialogue = new[] { "Selected\nAct 1." };
			}
			else if (command == "Act 2") {
				this.Dialogue = new[] { "Selected\nAct 2." };
			}
			else if (command == "Act 3") {
				this.Dialogue = new[] { "Selected\nAct 3." };
			}
			this.Battle.Dialog($"You selected {command}.");

		}


	}
}
