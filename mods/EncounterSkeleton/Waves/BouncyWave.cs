namespace EncounterSkeleton.Waves {
	using System;

	public class BouncyWave : CreateYourDot.Waves.Wave
	{

		public override float RealTimerMax => 4.0f;

		public override void Create() { }

		public override void Update(float delta) {
			if (this._FrameTimer % 30 == 0) {
				var posx = 30 - Godot.GD.Randf() * 60;
				var posy = this.Battle.Arena.Dimensions.y / 2;

				var p = this.SpawnProjectile<Projectiles.BouncyBullet>();
				p.x = posx;
				p.y = posy;

				p.velx = 1 - 2 * Godot.GD.Randf();
				p.vely = 0;
				// p.Position = new Godot.Vector2(
				// 	30 - Godot.GD.Randf() * 60,
				// 	// Diameter, not radius.
				// 	-p.Arena.Dimensions.y
				// );

			}

		}
	}
}
