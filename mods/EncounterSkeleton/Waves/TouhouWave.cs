namespace EncounterSkeleton.Waves {
	using System;

	public class TouhouWave : CreateYourDot.Waves.Wave
	{

		private const float _Y_OFFSET = 180;

		private float _mult = 0.5f;

		public override void Update(float delta) {

			if (this._FrameTimer % 30 == 0) {
				const int NUM_BULLETS = 10;
				for (int i = 0; i < NUM_BULLETS; i++) {
					var bullet = this.SpawnProjectile<Projectiles.TouhouBullet>();
					bullet.y = _Y_OFFSET;

					bullet.YOffset = _Y_OFFSET;

					bullet.Offset = MathF.PI * 2 * i / NUM_BULLETS;

					// `negmult` from the original Lua file doesn't seem to
					// do anything.

					bullet.Mult = this._mult;



				}
				this._mult += 0.05f;
			}

		}

	}
}
