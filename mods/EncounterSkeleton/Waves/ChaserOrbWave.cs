namespace EncounterSkeleton.Waves {
	public class ChaserOrbWave : CreateYourDot.Waves.Wave
	{

		public override void Create() {

			this.SpawnProjectile<Projectiles.ChaserOrb>();
		}

	}
}
