namespace EncounterSkeleton {
	public class TestItem : CreateYourDot.Inventory.Items.ConsumableItem
	{

		public override string Name => "Test Item";

		public override uint StackSize => 1;

		public override void Consume(CreateYourDot.Scenes.Battle.AbstractBattle battle) {

			battle.Player.Damage(1);
			battle.State("action_select");

		}
	}
}
