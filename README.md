# Create Your Dot

An implementation of [Create Your Frisk](https://github.com/RhenaudTheLukark/CreateYourFrisk/)
in Godot with C#.

It's a bit messy inside. Was made over the course of 3 or 4 days, and not a lot of effort went
into making it clean.

## Name

*CreateYour*Frisk plus Go*dot* plus *dot* Net equals *CreateYourDot*.

## Usage

Use Godot Mono, version 3.2.3. Open the project and start it.

## Mods

Mods are internal at the moment, because I couldn't care to dynamically link them.
See the `mods` directory.

The Battle scene just calls Encounter Skeleton's encounter, built-in.

Waves are just C# scripts that extend `CreateYourDot.Waves.Wave`. The Create method is called
once, at the beginning, while the Update method is called every physics tick.

Bullets are Godot scenes. The scene must extend Godot.Area2D and the script attached to the scene
must extend `CreateYourDot.Projectiles.Projectile`. They also have a Create and Update method.

## License

This reuses some assets and a bit of code from Create Your Frisk. Therefore, it is under the GNU General
Public License, version 3.
