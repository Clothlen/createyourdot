namespace CreateYourDot.Encounters {
	using System;
	using Godot;

	public abstract class Encounter
	{

		public abstract string Id { get; }

		public Modding.Mod ParentMod { get; internal set; }

		public Scenes.Battle.UT.Battle Battle { get; internal set; }

		public virtual string TurnText { get; set; } = "The developer forgot to set their turn text!";

		public virtual float WaveTimer { get; set; } = 4.0f;

		public bool CanFlee { get; set; } = false;

		public float FleeSuccessChance { get; set; } = 0.5f;

		public string[] FleeText { get; set; } = new[] { "You ran away." };

		public bool Revive { get; set; } = false;

		public string[] DeathText { get; set; } = new[] { "You died." };

		public Type[] NextWaves { get; set; } = new Type[0];

		internal void init(Scenes.Battle.UT.Battle battle) {
			this.Battle = battle;
		}

		// Similar to EncounterStarting
		public abstract void Create();

		public virtual void Update(float delta) { }

		public void AddEnemy<T>()
		where T : Enemies.Enemy, new() {
			this.Battle.AddEnemy<T>(this.ParentMod);
		}

		public void AddEnemy(string name, Vector2 position) {
			var instance = Modding.ModLoader.get_enemy(name);

			instance.Position += new Vector2(0, -155);
			// instance.Scale = new Vector2(0.5f, 0.5f);

			this.Battle.AddChild(instance);
		}

		public void AddEnemy(string location, float x, float y) {
			this.AddEnemy(location, new Vector2(x, y));
		}

		// Auto position
		public void AddEnemy(string location) {
			// TODO
			this.AddEnemy(location, new Vector2(0, 0));
		}

		public void StartWave<T>()
		where T : Waves.Wave, new() {

		}

		public virtual Waves.Wave GetNextWave() {
			var random_index = Util.RandomUtil.Random.Next(0, this.NextWaves.Length);
			var wave_type = this.NextWaves[random_index];

			return (Waves.Wave)Activator.CreateInstance(wave_type);

		}

	}
}
