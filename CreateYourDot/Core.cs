namespace CreateYourDot {
	public static class Core
	{

		public static readonly int PHYSICS_TICKRATE = (int)Godot.ProjectSettings.GetSetting("physics/common/physics_fps");

		internal static Scenes.CoreNode CoreNode { get; private set; }

		internal static void init(Scenes.CoreNode core_node) {

			CoreNode = core_node;

			Godot.GD.Randomize();
			Modding.ModLoader.load_all();
			Modding.ModLoader.register_encounters();

		}

		// Duh
		public const bool IsCYF = false;


	}
}
