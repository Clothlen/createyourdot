namespace CreateYourDot.Util {
	public static class PhysicsUtil
	{

		// Taken from Godot.Bitmap.CreateFromImageAlpha default argument.
		private const float BITMAP_CREATE_ALPHA_THRESHOLD_DEFAULT = 0.1f;

		// Taken from Godot.Bitmap.OpaqueToPolygons default argument.
		private const float BITMAP_OPAQUE_POLYGONS_EPSILON_DEFAULT = 2.0f;

		public static Godot.CollisionPolygon2D[] ConvertSpriteToColliders(Godot.Sprite sprite, float threshold = BITMAP_CREATE_ALPHA_THRESHOLD_DEFAULT, float epsilon = BITMAP_OPAQUE_POLYGONS_EPSILON_DEFAULT) {
			return ConvertSpriteToColliders(sprite.Texture.GetData());
		}

		public static Godot.CollisionPolygon2D[] ConvertSpriteToColliders(Godot.Texture texture, float threshold = BITMAP_CREATE_ALPHA_THRESHOLD_DEFAULT, float epsilon = BITMAP_OPAQUE_POLYGONS_EPSILON_DEFAULT) {
			return ConvertSpriteToColliders(texture.GetData());
		}

		public static Godot.CollisionPolygon2D[] ConvertSpriteToColliders(Godot.Image image, float threshold = BITMAP_CREATE_ALPHA_THRESHOLD_DEFAULT, float epsilon = BITMAP_OPAQUE_POLYGONS_EPSILON_DEFAULT) {
			var bm = new Godot.BitMap();
			bm.CreateFromImageAlpha(image, threshold);

			var polygons = bm.OpaqueToPolygons(new Godot.Rect2(0, 0, bm.GetSize()));

			var colliders = new Godot.CollisionPolygon2D[polygons.Count];

			for (int i = 0; i < polygons.Count; i++) {
				var polygon = (Godot.Vector2[])polygons[i];

				var collider = new Godot.CollisionPolygon2D();
				collider.Polygon = polygon;

				colliders[i] = collider;
			}

			return colliders;

		}

	}
}
