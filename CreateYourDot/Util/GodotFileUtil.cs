namespace CreateYourDot.Util {
	using Godot;

	public static class GodotFileUtil
	{

		public static bool Exists(string path) {
			// No need to free, File is a reference.
			return new File().FileExists(path);
		}

		public static bool IsEmbedded(string path) {
			return path.StartsWith("res://");
		}

		public static bool IsUser(string path) {
			return path.StartsWith("user://");
		}

		public static bool IsFree(string path) {
			return !(IsEmbedded(path) || IsUser(path));
		}

		public static string ReadEmbeddedAsText(string path) {
			var f = new File();
			f.Open(path, File.ModeFlags.Read);
			string text;
			try {
				text = f.GetAsText();
			}
			finally {
				f.Close();
			}
			return text;
		}

		public static byte[] ReadEmbeddedAsBytes(string path) {
			var f = new File();
			f.Open(path, File.ModeFlags.Read);

			var length = (int)f.GetLen();
			byte[] bytes;
			try {
				bytes = f.GetBuffer(length);
			}
			finally {
				f.Close();
			}

			return bytes;

		}

	}
}
