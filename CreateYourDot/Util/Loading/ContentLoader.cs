namespace CreateYourDot.Util.Loading {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Reflection;

	public class ContentLoader
	{

		/// <summary>
		/// This is our cache.
		/// </summary>
		/// <typeparam name="string"></typeparam>
		/// <typeparam name="Godot.Resource"></typeparam>
		/// <returns></returns>
		protected readonly Dictionary<string, Godot.Resource> _Paths = new Dictionary<string, Godot.Resource>();

		private const bool NO_CACHE = false;

		/// <summary>
		/// Unload and clear the cache.
		/// </summary>
		public void Unload() {
			foreach (var obj in this._Paths.Values) {
				if (obj is Godot.Reference) {
					// Can't free references.
					continue;
				}
				obj.Free();
			}
			this._Paths.Clear();
		}

		/// <summary>
		/// Load a given resource and put it into the cache.
		/// </summary>
		/// <param name="path">The path to the resource. Accepts Godot-paths.</param>
		/// <param name="type_hint">Optional type hint. See Godot.ResourceLoader.Load.</param>
		/// <returns>The requested resource.</returns>
		public Godot.Resource Load(string path, string? type_hint = null) {
			if (!this._Paths.ContainsKey(path)) {
				var loaded = Godot.ResourceLoader.Load(path, type_hint, NO_CACHE);
				this._Paths.Add(path, loaded);
			}

			return this._Paths[path];
		}

		/// <summary>
		/// See #Load, but accepting an autocasted type.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="type_hint"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T Load<T>(string path, string? type_hint = null)
		where T : Godot.Resource {
			return (T)this.Load(path, type_hint);
		}

		/// <summary>
		/// Create an instance of a scene.
		///
		/// The given type must provide a <see cref="ScenePathAttribute"/>, or
		/// you must provide the path as an argument.
		/// </summary>
		/// <typeparam name="T">The type to create.</typeparam>
		/// <returns></returns>
		public T Instance<T>()
		where T : Godot.Node {
			var path = this.GetScenePath<T>();
			if (path is null) {
				throw new ArgumentException($"Scene did not provide a path, and neither did caller. Type: {typeof(T).FullName}");
			}

			return this.Instance<T>(path);
		}

		public Godot.Node Instance(string path) {
			var scene = this.Load<Godot.PackedScene>(path);
			return scene.Instance();
		}

		public T Instance<T>(string path)
		where T : Godot.Node {
			var scene = this.Load<Godot.PackedScene>(path);
			return (T)scene.Instance();
		}

		/// <summary>
		/// Get the scene path of a given scene type.
		///
		/// The type must provide <see cref="ScenePathAttribute"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns>The path to the scene file.</returns>
		public string? GetScenePath(Type type) {
			var attribute = type.GetCustomAttribute<ScenePathAttribute>(false);

			var path = attribute?.Path;
#if DEBUG
			if (!(path is null)) {
				Debug.Assert(GodotFileUtil.Exists(path));
			}
#endif
			return path;
		}

		public string? GetScenePath<T>()
		where T : Godot.Node {
			return this.GetScenePath(typeof(T));
		}
	}
}
