namespace CreateYourDot.Util.Loading {
	using System;

	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	public sealed class ScenePathAttribute : Attribute
	{

		public string Path { get; private set; }

		public ScenePathAttribute(string path) {
			this.Path = path;
		}

	}
}
