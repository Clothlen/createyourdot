namespace CreateYourDot.Util.States {
	using System;

	public abstract class AbstractState
	{

		public abstract string Name { get; }

		protected StateMachine _StateMachine;

		internal void init(StateMachine sm) {
			this._StateMachine = sm;
		}

		public void Push(string name) {
			this._StateMachine.Push(name);
		}

		public void Pop() {
			if (this._StateMachine.Current != this.Name) {
				throw new InvalidOperationException("Tried to pop self state, but wasn't at the top of the stack.");
			}

			this._StateMachine.Pop();
		}

		public void Transfer(string? to) {
			this._StateMachine.Transfer(this.Name, to);
		}

		public virtual void Enter(string? from) { }

		public virtual void Exit(string? to) { }

		public virtual void Update(float delta) { }

		public virtual void Input(Godot.InputEvent evt) { }

	}
}
