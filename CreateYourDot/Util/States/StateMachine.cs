namespace CreateYourDot.Util.States {
	using System;
	using System.Collections.Generic;
	using System.Collections.Immutable;
	using System.Collections.ObjectModel;
	using System.Diagnostics;

	public class StateMachine
	{

		public ReadOnlyDictionary<string, AbstractState> States;
		public int ChangeCount { get; protected set; } = 0;

		private readonly Dictionary<string, AbstractState> _states = new Dictionary<string, AbstractState>();
		private readonly List<string> _stack = new List<string>();

		public StateMachine() {
			this.States = new ReadOnlyDictionary<string, AbstractState>(this._states);
		}

		public StateMachine(params AbstractState[] states) : base() {
			this.Add(states);
		}

		public void Add(AbstractState state) {
			this._add(state);
		}

		public void Add(params AbstractState[] states) {
			foreach (var state in states) {
				this._add(state);
			}
		}

		public bool Has(string name) {
			return this._states.ContainsKey(name);
		}

		public bool Has(AbstractState state) {
			return this._states.ContainsValue(state);
		}

		public string? Previous {
			get {
				try {
					return this._stack[^2];
				}
				catch (ArgumentOutOfRangeException) {
					return null;
				}
			}
		}

		public string? Current {
			get {
				try {
					return this._stack[^1];
				}
				catch (ArgumentOutOfRangeException) {
					return null;
				}
			}
		}

		public void Push(string name) {
			Debug.Assert(this.Has(name), "Tried to push a new state to the stack that doesn't exist.");

			var old_current = this.Current;
			this._stack.Add(name);
			this.ChangeCount++;
			var new_current = this.Current;

			if (old_current != null) {
				this._states[old_current].Exit(new_current);
			}

			if (new_current != null) {
				this._states[new_current].Enter(old_current);
			}

		}

		public void Pop() {
			var old_current = this.Current;
			this._try_pop_end();
			var new_current = this.Current;

			if (old_current != null) {
				this._states[old_current].Exit(new_current);
			}

			if (new_current != null) {
				this._states[new_current].Enter(old_current);
			}
		}

		public void Transfer(string current, string? to) {
			// We can assume that this method will only be called if a state is already in the stack.
			this._stack.RemoveAt(this._stack.Count - 1);
			this.ChangeCount++;
			this._states[current].Exit(to);

			if (to != null) {
				Debug.Assert(this.Has(to));
				this._stack.Add(to);
				// Don't track ChangeCount here.

				this._states[to].Enter(current);
			}
		}

		public void Start(string name) {
			if (this.Current != null) {
				throw new InvalidOperationException("State machine has already started.");
			}

			if (this._stack.Count > 0) {
				throw new InvalidOperationException("State machine has already started and has a stack.");
			}

			this.Push(name);

		}

		public void Update(float delta) {
			this._states[this._stack[^1]].Update(delta);
		}

		public void Input(Godot.InputEvent evt) {
			this._states[this._stack[^1]].Input(evt);
		}

		private void _add(AbstractState state) {
			this._states.Add(state.Name, state);
			state.init(this);
		}

		private void _try_pop_end() {
			try {
				this._stack.RemoveAt(this._stack.Count - 1);
				this.ChangeCount++;
			}
			catch (ArgumentOutOfRangeException) {
				// Do nothing. This only occurs if the length is zero.
				// We don't care if this happens, because it's rare.
			}
		}


	}
}
