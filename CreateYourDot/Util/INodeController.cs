namespace CreateYourDot.Util {
	public interface INodeController<T>
	where T : Godot.Node
	{
		T Node { get; }
	}

	public interface INodeController : INodeController<Godot.Node>
	{ }

}
