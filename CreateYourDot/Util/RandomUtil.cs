namespace CreateYourDot.Util {
	using System;
	using System.Collections.Generic;
	using System.Linq;

	public static class RandomUtil
	{

		public static readonly Random Random = new Random();

		public static bool RandomBool() {
			return RandomBool(Random);
		}

		public static bool RandomBool(Random random) {
			return random.NextDouble() >= 0.5;
		}

		public static Godot.Vector2 RandomDirectionalVector() {
			return new Godot.Vector2(
				// Set it to either -1, 0, or 1.
				// This gives it a random direction.
				// Floats do not look great, so this is
				// just integers.
				// Add 1, because of directional.
				Random.Next(0, 2 + 1) - 1,
				Random.Next(0, 2 + 1) - 1
			);
		}

		public static T Choose<T>(IEnumerable<T> val) {
			return val.ElementAt(
				Random.Next(0, val.Count())
			);
		}

	}
}
