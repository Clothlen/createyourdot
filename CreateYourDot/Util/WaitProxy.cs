namespace CreateYourDot.Util {
	public static class WaitProxy
	{

		public delegate void CallbackDelegate();

		public static Godot.Timer GetTimerWithCallback(CallbackDelegate func) {
			var timer = new Godot.Timer();

			var proxy = new Proxy(timer, func);
			timer.AddChild(proxy);

			return timer;
		}

		private sealed class Proxy : Godot.Node
		{

			private readonly Godot.Timer _timer;

			private readonly CallbackDelegate _callback;

			public Proxy(Godot.Timer timer, CallbackDelegate callback) {
				this._timer = timer;
				this._callback = callback;

				this._timer.Connect("timeout", this, nameof(this._finished));
			}

			private void _finished() {
				this._timer.GetParent()?.RemoveChild(this._timer);
				this._timer.QueueFree();
				this._callback();

				// No need to free ourselves, as calling QueueFree on this._timer
				// will free its children (which includes us).
			}

		}

	}
}
