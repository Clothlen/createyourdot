namespace CreateYourDot.Loaders {

	using System.Collections.Generic;
	using Godot;

	// The reason for this is that Godot's ResourceLoader cannot be cleared.
	internal static class ImageLoader
	{

		// Key refers to path to image
		private static readonly Dictionary<string, Image> _image_cache = new Dictionary<string, Image>();
		// Key refers to path to image
		private static readonly Dictionary<string, ImageTexture> _image_texture_cache = new Dictionary<string, ImageTexture>();

		internal static Image get_image(string path) {
			if (!_image_cache.ContainsKey(path)) {
				var image = new Image();
				image.Load(path);
				_image_cache.Add(path, image);
			}

			return _image_cache[path];
		}

		internal static ImageTexture get_image_texture(string path) {
			if (!_image_texture_cache.ContainsKey(path)) {
				var image = get_image(path);
				var tex = new ImageTexture();
				tex.CreateFromImage(image);
				_image_texture_cache.Add(path, tex);
			}

			return _image_texture_cache[path];
		}

		internal static void clear() {
			// Images are reference-counted by Godot and automatically freed.
			_image_cache.Clear();
		}

	}
}
