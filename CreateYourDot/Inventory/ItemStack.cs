namespace CreateYourDot.Inventory {
	using System;
	using System.Diagnostics.Contracts;

	/// <summary>
	/// An item container.
	///
	/// It contains stackable and non-stackable items.
	/// </summary>
	public class ItemStack<T>
	where T : Items.AbstractItem
	{

		/// <summary>
		/// The amount of <see cref="Items.AbstractItem"/> in the stack.
		///
		/// If it is zero, assume it is either not initialized or invalid.
		/// </summary>
		public uint Amount { get; set; } = 1;

		/// <summary>
		/// The item that is being stored.
		/// </summary>
		public T Item { get; set; }

		public ItemStack(T item, uint amount = 1) {
			Contract.Requires(amount >= 0);
			Contract.Requires(amount <= item.StackSize);
			Contract.EndContractBlock();

			this.Item = item;
			this.Amount = amount;
		}

		/// <summary>
		/// Determine if another ItemStack has the same kind of <see cref="Items.AbstractItem"/>.
		/// </summary>
		/// <param name="other">The ItemStack to compare to.</param>
		/// <returns>True if the same, else false.</returns>
		public bool IsSameKind(ItemStack other) {
			return this.Item == other.Item;
		}

		/// <summary>
		/// Remove a number of <see cref="Items.AbstractItem"/> from the stack.
		/// Generates a new ItemStack with the taken items.
		///
		/// Throws System.ArgumentOutOfRangeException if you try to take more items
		/// out of the stack than there are items in the stack.
		/// </summary>
		/// <param name="count">The amount to take.</param>
		/// <returns>A new ItemStack.</returns>
		public ItemStack<T> Take(uint count) {
			Contract.Requires(count > 0);
			if (count > this.Amount) {
				throw new ArgumentOutOfRangeException("count", $"Tried to take {count} items when there are only {this.Amount} items.");
			}
			Contract.EndContractBlock();

			this.Amount -= count;

			return new ItemStack<T>(
				this.Item,
				count
			);

		}

		/// <summary>
		/// Add new <see cref="Items.AbstractItem"/>s to the stack.
		///
		/// If the result of the addition is higher than the maximum stack size of
		/// the item, a new ItemStack is returned. Otherwise, null is returned.
		/// </summary>
		/// <param name="count">The items to add.</param>
		/// <returns>Null, or a new ItemStack.</returns>
		public ItemStack<T>? Give(uint count) {
			Contract.Requires(count > 0);
			Contract.EndContractBlock();

			uint adder = this.Amount + count;
			if (adder > this.Item.StackSize) {
				var difference = adder - this.Item.StackSize;
				Contract.Assert(difference > 0);

				return new ItemStack<T>(this.Item, difference);
			}

			return null;

		}

		/// <summary>
		/// Combine two ItemStacks to create one ItemStack.
		///
		/// Both of the ItemStacks must have the same <see cref="Items.AbstractItem"/>.
		///
		/// If the result of the addition is higher than the maximum stack size of
		/// the item, a new ItemStack is returned. Otherwise, null is returned.
		/// </summary>
		/// <param name="other">The other ItemStack to combine into.</param>
		/// <returns>Null, or a new ItemStack.</returns>
		public ItemStack<T>? Combine(ItemStack other) {
			Contract.Requires(this.Valid);
			Contract.Requires(other.Valid);
			if (!this.IsSameKind(other)) {
				throw new ArgumentException("ItemStacks must be the same kind of Item to combine.");
			}
			Contract.EndContractBlock();

			var amt = other.Amount;
			other.Destroy();
			return this.Give(amt);
		}

		/// <summary>
		/// Split an ItemStack in half.
		///
		/// This ItemStack will have its count halved, without any remainder. A
		/// new ItemStack will be returned, with half the original count plus
		/// any remainder.
		///
		/// If the original count is less than 1, the original ItemStack will be destroyed.
		/// </summary>
		/// <returns>Null, or a new ItemStack.</returns>
		public ItemStack<T> Split() {
			uint divided = this.Amount / 2;
			uint first_half = divided;
			uint second_half = divided + (this.Amount % 2);

			if (first_half > 1) {
				this.Amount = first_half;
				var new_stack = new ItemStack<T>(this.Item, second_half);
				return new_stack;
			}
			else {
				var item = this.Item;
				this.Destroy();
				return new ItemStack<T>(item, second_half);
			}

		}

		/// <summary>
		/// Destroy the ItemStack.
		///
		/// It is rendered invalid and should not be used anymore.
		/// </summary>
		public void Destroy() {
			Contract.Requires(!this.Valid);
			Contract.EndContractBlock();

			this.Amount = 0;
			this.Valid = false;
		}

		/// <summary>
		/// Determines if the ItemStack is still valid or not.
		///
		/// If an ItemStack is not valid, it should not be used
		/// and should be removed.
		/// </summary>
		/// <value>True if valid.</value>
		public bool Valid { get; set; }

	}

	public class ItemStack : ItemStack<Items.AbstractItem> {

		public ItemStack(Items.AbstractItem item, uint amount = 1) : base(item, amount) { }

	}
}
