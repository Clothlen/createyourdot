namespace CreateYourDot.Inventory.Items {
	public abstract class ConsumableItem : UsableItem
	{

		public override void Use(Scenes.Battle.AbstractBattle battle) {
			base.Use(battle);
			this.Consume(battle);
		}

		public virtual void Consume(Scenes.Battle.AbstractBattle battle) { }

	}
}
