namespace CreateYourDot.Inventory.Items.Weapons {
	public class ToyKnife : AbstractWeapon
	{

		public override string Name => "toy_knife";

		public override uint StackSize => 1;

		public override float Attack { get; set; } = 3;
	}
}
