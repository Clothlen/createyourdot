namespace CreateYourDot.Inventory.Items {
	public abstract class UsableItem : AbstractItem
	{
		// We can have an overload that uses Overworld for example.
		public virtual void Use(Scenes.Battle.AbstractBattle battle) { }

	}
}
