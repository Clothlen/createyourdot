namespace CreateYourDot.Inventory.Items {
	public abstract class AbstractWeapon : UsableItem
	{

		public abstract float Attack { get; set; }

	}
}
