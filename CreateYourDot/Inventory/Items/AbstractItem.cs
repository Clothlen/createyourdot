namespace CreateYourDot.Inventory.Items {
	public abstract class AbstractItem
	{

		public abstract uint StackSize { get; }

		public abstract string Name { get; }

	}
}
