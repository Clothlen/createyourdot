namespace CreateYourDot.Inventory {
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Diagnostics.Contracts;
	using Items;

	/// <summary>
	/// An inventory is any container that contains ItemStacks.
	///
	/// This could be a player's inventory, or a box of some kind.
	/// </summary>
	public abstract class AbstractInventory : IEnumerable<ItemStack?>
	{

		/// <summary>
		/// How full the inventory is.
		/// </summary>
		/// <value>How full it is.</value>
		public virtual uint Fullness { get; protected set; }

		/// <summary>
		/// The maximum number of slots this inventory has.
		///
		/// See <see cref="Resize"/> to change this.
		/// </summary>
		/// <value>Positive Integer.</value>
		public virtual uint Capacity { get; protected set; }

		/// <summary>
		/// The maximum size of any stack of this inventory.
		///
		/// This overrides <see cref="ItemStack"/> stack size, but only as a maximum.
		/// </summary>
		/// <value>Positive integer.</value>
		public virtual uint MaxStackSize { get => uint.MaxValue; }

		private ItemStack?[] _stacks;

		public AbstractInventory() {
			this._stacks = new ItemStack?[this.Capacity];
		}

		/// <summary>
		/// Add a new <see cref="ItemStack"/> to the first empty slot it can find.
		/// </summary>
		/// <param name="stack"></param>
		public virtual void Add(ItemStack stack) {
			if (this.Fullness >= this.Capacity) {
				throw new FullInventoryException();
			}
			Contract.EndContractBlock();

			var index = (int)this.FirstEmptySlot!;
			this[index] = stack;
		}

		/// <summary>
		/// Add a new <see cref="AbstractItem"/> to the first empty slot it can find.
		/// </summary>
		/// <param name="item">The item to add.</param>
		/// <param name="amount">The amount of items to put in the slot.</param>
		public virtual void Add(AbstractItem item, uint amount = 1) {
			var stack = new ItemStack(item, amount);
			this.Add(stack);
		}

		/// <summary>
		/// Take an <see cref="ItemStack"/> out of the inventory.
		/// </summary>
		/// <param name="index">The index to take it from.</param>
		/// <returns>The stored ItemStack.</returns>
		public virtual ItemStack? Take(uint index) {
			Contract.Requires<ArgumentOutOfRangeException>(index >= 0 && index < this.Capacity);
			Contract.EndContractBlock();

			var stack = this._stacks[index];
			this._stacks[index] = null;
			return stack;
		}

		/// <summary>
		/// Take all <see cref="ItemStack"/>s out of the inventory.
		/// </summary>
		/// <returns>All ItemStacks in the inventory.</returns>
		public virtual ItemStack?[] TakeAll() {
			var returner = new ItemStack?[this._stacks.Length];

			for (int i = 0; i < this._stacks.Length; i++) {
				returner[i] = this._stacks[i];
				this._stacks[i] = null;
			}

			return this._stacks;
		}

		/// <summary>
		/// Destroy an item at an index.
		/// </summary>
		/// <param name="index"></param>
		public virtual void Destroy(uint index) {
			this._stacks[index]?.Destroy();
			this._stacks[index] = null;
		}

		/// <summary>
		/// Destroy all items in the inventory, clearing it.
		/// </summary>
		public virtual void DestroyAll() {
			for (uint i = 0; i < this._stacks.Length; i++) {
				this.Destroy(i);
			}
		}

		/// <summary>
		/// Resize an inventory, shrinking or growing it.
		///
		/// Raises System.ArgumentOutOfRangeException if the size is less than 0.
		///
		/// Raises System.ArgumentOutOfRangeException if the new size would lose items.
		/// Items must be destroyed and set to null first.
		/// </summary>
		/// <param name="size">The new size.</param>
		public virtual void Resize(int size) {
			Contract.Requires<ArgumentOutOfRangeException>(size >= 0);

			// If we shrink an inventory, what happens to items that are lost?
			// We should throw an error if that happens.
			// if (size <= this.Fullness) {
			if (size > this.LastEmptySlot) {
				throw new ArgumentOutOfRangeException("Inventory resizing must handle lost ItemStacks.");
			}

			Contract.EndContractBlock();

			Array.Resize(ref this._stacks, size);

		}

		/// <summary>
		/// Determines if a given <see cref="ItemStack"/> is within the inventory.
		/// </summary>
		/// <param name="stack"></param>
		/// <returns></returns>
		public virtual bool Contains(ItemStack stack) {
			foreach (var s in this._stacks) {
				if (s == null) {
					continue;
				}

				if (s == stack) {
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Determines if the given <see cref="AbstractItem"/> is within any of the stacks within the inventory.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public virtual bool Contains(AbstractItem item) {
			foreach (var s in this._stacks) {
				if (s == null) {
					continue;
				}

				if (s.Item == item) {
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Determines if a given <see cref="ItemStack"/> can be added into any slot in the inventory.
		///
		/// If true, this means that at least one of the ItemStack can be inserted into at least one slot.
		/// </summary>
		/// <param name="stack"></param>
		/// <returns></returns>
		public virtual bool Stackable(ItemStack stack) {
			if (!this.Full) {
				return true;
			}

			foreach (var s in this._stacks) {
				if (s == null) {
					continue;
				}

				if (s.IsSameKind(stack) && s.Amount < s.Item.StackSize) {
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Adds a given <see cref="ItemStack"/> into the inventory, if possible.
		///
		/// Will combine into any same ItemStack if space is available. Otherwise, it will stack
		/// into empty slots.
		///
		/// If there is no space available, the remainder is returned as a new ItemStack. Otherwise, null is returned.
		/// </summary>
		/// <param name="stack">The ItemStack to insert.</param>
		/// <returns>Null, or any leftover ItemStack that could not be inserted.</returns>
		public virtual ItemStack? StackInto(ItemStack stack) {
			ItemStack? working_stack = stack;
			for (int i = 0; i < this._stacks.Length; i++) {
				var s = this._stacks[i];

				if (s == null) {
					s = new ItemStack(stack.Item, 0);
					s.Amount = 0;
					this._stacks[i] = s;
					working_stack = s;
				}
				else {
					if (!s.IsSameKind(working_stack)) {
						continue;
					}

					working_stack = s.Combine(working_stack) as ItemStack;
					if (working_stack == null || !working_stack.Valid) {
						break;
					}
				}
			}

			return working_stack;
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}

		public virtual IEnumerator<ItemStack?> GetEnumerator() {
			foreach (var stack in this._stacks) {
				yield return stack;
			}
		}

		/// <summary>
		/// Determine if the inventory is full.
		/// </summary>
		/// <value>True if it is full.</value>
		public virtual bool Full {
			get => this.Fullness == this.Capacity;
		}

		/// <summary>
		/// The first slot in the inventory that is empty.
		///
		/// Returns null if the inventory is full.
		/// </summary>
		/// <value>The index of the first empty slot.</value>
		public virtual int? FirstEmptySlot {
			get {
				for (int i = 0; i < this._stacks.Length; i++) {
					if (this._stacks[i] == null) {
						return i;
					}
				}
				return null;
			}
		}

		/// <summary>
		/// The last slot in the inventory that is empty.
		///
		/// Returns null if the inventory is full.
		/// </summary>
		/// <value></value>
		public virtual int? LastEmptySlot {
			get {
				for (int i = this._stacks.Length - 1; i >= 0; i--) {
					if (this._stacks[i] == null) {
						return i;
					}
				}
				return null;
			}
		}

		public ItemStack? this[int index] {
			get => this._stacks[index];
			set {
				if (value == null) {
					this._stacks[index] = null;
					this.Fullness--;
					return;
				}

				var old = this._stacks[index];

				if (old != null) {
					throw new SlotAlreadyFilledException("Tried to set a stack at a non-null index.");
				}

				this._stacks[index] = value;
				this.Fullness++;
			}
		}

		[Serializable]
		public sealed class FullInventoryException : Exception
		{
			public FullInventoryException() : base("Inventory is full.") { }
		}

		[Serializable]
		public sealed class SlotAlreadyFilledException : Exception
		{
			public SlotAlreadyFilledException(string msg) : base($"Slot is already filled: {msg}") { }
		}

	}
}
