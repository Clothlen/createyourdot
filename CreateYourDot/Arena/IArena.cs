namespace CreateYourDot.Arena {
	public interface IArena : Util.INodeController<Godot.Node2D>
	{

		Godot.Vector2 Position { get; set; }

		Godot.Vector2 Dimensions { get; set; }

		bool Active { get; set; }
	}
}
