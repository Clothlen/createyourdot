namespace CreateYourDot.Arena {
	public abstract class AbstractArena : IArena, IBorderedArena
	{

		protected readonly Scenes.Battle.ArenaProxy _Node = new Scenes.Battle.ArenaProxy();

		private bool _active = false;

		// private uint? _old_layer_mask;
		private uint _old_collision_mask;
		private uint _old_layers;

		public AbstractArena() {
			this._Node.CollisionLayer = 0b00000000000000000100;
			this._Node.CollisionMask =  0b00000000000000000010;
		}

		public Godot.Node2D Node => this._Node;

		public Godot.Vector2 Position {
			get => this.Node.Position;
			set {
				this.Node.Position = value;
			}
		}

		public abstract Godot.Vector2 Dimensions { get; set; }

		public abstract Godot.Vector2 InnerDimensions { get; set; }

		public bool Active {
			get => this._active;
			set {
				this._active = value;

				if (value) {
					this._Node.Show();

					this._Node.Layers = this._old_layers;
					this._Node.CollisionMask = this._old_collision_mask;
					this._old_layers = 0;
					this._old_collision_mask = 0;
				}
				else {
					this._Node.Hide();

					this._old_layers = this._Node.Layers;
					this._old_collision_mask = this._Node.CollisionMask;
					this._Node.Layers = 0;
					this._Node.CollisionMask = 0;
				}
			}
		}

		// public abstract bool Disabled {
		// 	get => this._disabled;
		// 	set;
		// }

	}
}
