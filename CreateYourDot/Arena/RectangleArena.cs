namespace CreateYourDot.Arena {
	using Godot;

	public class RectangleArena : AbstractArena
	{

		public override Vector2 Dimensions { get; set; }

		public override Vector2 InnerDimensions { get; set; }

		private readonly Rect2[] _rects;

		public RectangleArena(int x, int y, int thickness) : this(new Vector2(x, y), thickness) { }

		public RectangleArena(Vector2 outer_dimensions, int thickness) {
			var inner_dimensions = outer_dimensions - new Vector2(thickness * 2, thickness * 2);

			this.Dimensions = outer_dimensions;
			this.InnerDimensions = inner_dimensions;

			this._rects = new Rect2[] {
				new Rect2(0, -outer_dimensions.y + thickness, outer_dimensions.x, thickness),
				new Rect2(0, outer_dimensions.y - thickness, outer_dimensions.x, thickness),
				new Rect2(-outer_dimensions.x + thickness, 0, thickness, inner_dimensions.y),
				new Rect2(outer_dimensions.x - thickness, 0, thickness, inner_dimensions.y)
			};

			foreach (var rect in this._rects) {
				_mk_wall(this._Node, rect.Position, rect.Size);
			}

			this._Node.Drawer = this._drawer;

		}

		private void _drawer(Node2D node) {
			foreach (var rect in this._rects) {
				node.DrawRect(
					// We have to weird extra positioning here because it uses different anchoring,
					// and its radius is different.
					new Rect2(
						(rect.Position.x - rect.Size.x) / 2,
						(rect.Position.y - rect.Size.y) / 2,
						rect.Size.x,
						rect.Size.y
					),
					new Color(1.0f, 1.0f, 1.0f, 1.0f), true
				);
			}
		}

		private static void _mk_wall(PhysicsBody2D body, Vector2 pos, Vector2 dim) {
			var collider = new CollisionShape2D() {
				Shape = new RectangleShape2D() {
					Extents = dim * 0.5f
				}
			};

			collider.Position = pos * 0.5f;
			body.AddChild(collider);
		}

	}
}
