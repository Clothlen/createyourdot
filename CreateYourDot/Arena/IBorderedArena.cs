namespace CreateYourDot.Arena {
	public interface IBorderedArena
	{

		Godot.Vector2 InnerDimensions { get; set; }

	}
}
