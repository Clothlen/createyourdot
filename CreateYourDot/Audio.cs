namespace CreateYourDot {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;

	public sealed class Audio : Godot.Object
	{

		public string MainMusic { get; set; } = DEFAULT_MUSIC;

		public float PlayTime { get; set; }

		public float TotalTime { get; set; }

		public static readonly string DEFAULT_MUSIC = "res://resources/audio/mus_battle1.ogg";

		private readonly List<Godot.AudioStreamPlayer> _players = new List<Godot.AudioStreamPlayer>();

		private Godot.AudioStreamPlayer? _main_player;

		public void Play() {
			Debug.Assert(!(this._main_player is null));

			this._main_player.Play();
		}

		public void Stop() {
			Debug.Assert(!(this._main_player is null));

			this._main_player.Stop();
		}

		public void Pause() {
			Debug.Assert(!(this._main_player is null));

			this._main_player.Playing = false;
		}

		public void Unpause() {
			Debug.Assert(!(this._main_player is null));

			this._main_player.Playing = true;
		}

		public void Volume(float value) {
			Debug.Assert(!(this._main_player is null));

			var value_db = Godot.GD.Linear2Db(value);
			this._main_player.VolumeDb = value_db;
		}

		public void Pitch(float value) {
			Debug.Assert(!(this._main_player is null));

			this._main_player.PitchScale = value;
		}

		public void LoadFile(string path) {
			if (!(this._main_player is null)) {
				Core.CoreNode.RemoveChild(this._main_player);
			}

			this._main_player = new Godot.AudioStreamPlayer();
			this._main_player.Name = "AudioMainMusic";

			var volume_db = Godot.GD.Linear2Db(0.75f);
			this._main_player.VolumeDb = volume_db;
			this._main_player.Autoplay = true;

			var stream = Godot.ResourceLoader.Load<Godot.AudioStream>(path, null, true);

			// For WAV files.
			if (stream is Godot.AudioStreamSample wav) {
				wav.LoopMode = Godot.AudioStreamSample.LoopModeEnum.Forward;
			}

			this._main_player.Stream = stream;
			Core.CoreNode.AddChild(this._main_player);
		}

		public void PlaySound(string filepath, float volume = 0.65f) {
			// Debug.Assert(Godot.File(filepath));
			var audio = new Godot.AudioStreamPlayer();

			var volume_db = Godot.GD.Linear2Db(volume);
			audio.VolumeDb = volume_db;
			audio.Autoplay = true;
			var stream = Godot.ResourceLoader.Load<Godot.AudioStream>(filepath, null, true);

			// Not needed with WAV files.
			if (stream is Godot.AudioStreamOGGVorbis ogg) {
				ogg.Loop = false;
			}

			audio.Stream = stream;
			audio.Connect("finished", this, nameof(this._on_audio_finished), new Godot.Collections.Array() { audio });

			Core.CoreNode.AddChild(audio);

		}

		public void StopAll() {
			foreach (var a in this._players) {
				a.Stop();
			}
			this._main_player?.Stop();
		}

		public void PauseAll() {
			foreach (var a in this._players) {
				a.Playing = false;
			}

			if (!(this._main_player is null)) {
				this._main_player.Playing = false;
			}

		}

		public void UnpauseAll() {
			foreach (var a in this._players) {
				a.Playing = true;
			}

			if (!(this._main_player is null)) {
				this._main_player.Playing = true;
			}

		}

		public string GetSoundDictionary(string key) {
			throw new NotImplementedException();
		}

		public void SetSoundDictionary(string key, string value) {
			throw new NotImplementedException();
		}

		public string this[string key] {
			get {
				return this.GetSoundDictionary(key);
			}
			set {
				this.SetSoundDictionary(key, value);
			}
		}

		public bool IsPlaying {
			get {
				if (this._main_player is null) {
					// TODO: which is better behavior?
					// throw new ArgumentNullException("Couldn't get main player playing state, because the main player is null.");
					return false;
				}

				return this._main_player.Playing;
			}
			set {
				if (this._main_player is null) {
					throw new ArgumentNullException("Couldn't set main player playing state, because the main player is null.");
				}
				this._main_player.Playing = value;
			}
		}

		public bool IsValid => !(this._main_player is null);

		private void _on_audio_finished(Godot.AudioStreamPlayer player) {
			Core.CoreNode.RemoveChild(player);
		}

	}
}
