namespace CreateYourDot.Waves {
	using System;
	using System.Collections.Generic;
	using System.Timers;

	public abstract class Wave
	{

		public Scenes.Battle.AbstractBattle Battle { get; set; }

		public Modding.Mod Mod { get; set; }

		// In seconds
		public virtual float RealTimerMax { get; protected set; } = float.MaxValue;

		// public virtual double FrameTimerMax { get; protected set; } = int.MaxValue;

		protected float _FrameTimer { get; private set; } = 0;

		// protected double _FrameTimeEndingAt { get; private set; }

		protected Godot.Timer _TimerReal { get; private set; }

		protected readonly List<Projectiles.IProjectileController> _Projectiles = new List<Projectiles.IProjectileController>();

		internal void internal_create() {
			this._TimerReal = Util.WaitProxy.GetTimerWithCallback(this._on_end_wave_timer);
			this._TimerReal.WaitTime = this.RealTimerMax;
			Core.CoreNode.AddChild(this._TimerReal);

			this.Create();

			this._TimerReal.Start();
		}

		internal void internal_update(float delta) {
			this._FrameTimer++;

			// if (this._SpawnTimer > this._TimeEndingAt) {
			// 	this.End();
			// 	return;
			// }

			foreach (var proj in this._Projectiles) {
				proj.Update(delta);
			}

			this.Update(delta);
		}

		public void End() {
			this.OnEnd();
			for (int i = this._Projectiles.Count - 1; i >= 0; i--) {
				var projectile = this._Projectiles[i];
				if (!projectile.Persistant) {
					projectile.Node.GetParent().RemoveChild(projectile.Node);
					projectile.Node.QueueFree();
					this._Projectiles.RemoveAt(i);
				}
			}

			this.Battle.EndWave(this);

		}

		public virtual void Create() { }

		public virtual void Update(float delta) { }

		public virtual void OnEnd() { }

		public T SpawnProjectile<T>()
		where T : Projectiles.AbstractProjectileController, new() {
			var p = new T();
			p.Battle = this.Battle;
			p.Arena = this.Battle.Arena;

			var n = p.Node;
			if (n is Godot.PhysicsBody2D physics) {
				physics.CollisionLayer = 0b00000000000000001000;
				physics.CollisionMask = 0b00000000000000000010;
			}

			p.Create();

			this._Projectiles.Add(p);

			this.Battle.Layers["Bullet"].AddChild(p.Node);

			return p;
		}

		private void _on_end_wave_timer() {
			this._TimerReal.GetParent()?.RemoveChild(this._TimerReal);
			this._TimerReal.QueueFree();
			this.End();
		}

	}
}
