namespace CreateYourDot.Modding {
	using System.Collections.Generic;
	using Godot;

	public abstract class Mod
	{

		internal const string MOD_ROOT_DIRECTORY = "res://mods";

		public abstract string Id { get; }

		internal string mod_root { get { return $"{MOD_ROOT_DIRECTORY}/{this.Id}"; } }

		// public abstract void Battle(Scenes.Battle.Battle battle);

		// private List<Scenes.Battle.Objects.Monster> _monsters = new List<Scenes.Battle.Objects.Monster>();

		private List<string> _monster_paths = new List<string>();

		public void RegisterMonster(string path){
			// var monster = (Scenes.Battle.Objects.Monster)ResourceLoader.Load<PackedScene>(
			// 	$"{this.mod_root}/{path}.tscn"
			// ).Instance();
			// this._monsters.Add(monster);

			this._monster_paths.Add($"{this.mod_root}/Monsters/{path}.tscn");

		}

		public void AddEncounter(Encounters.Encounter encounter) {
			ModLoader.add_encounter(this.Id, encounter);
		}

		public virtual void RegisterEncounters(Registers.EncounterRegister register) { }

		internal void battle(Scenes.Battle.UT.Battle battle) {
			foreach (var monster_path in this._monster_paths) {
				var monster = (Scenes.Battle.Objects.Enemy)ResourceLoader.Load<PackedScene>(monster_path).Instance();

				battle.AddChild(monster);
			}
		}

	}
}
