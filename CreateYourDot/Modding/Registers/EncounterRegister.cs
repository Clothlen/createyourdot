namespace CreateYourDot.Modding.Registers {
	public sealed class EncounterRegister : IRegister
	{

		private readonly Mod _target;

		private readonly Registries.EncounterRegistry _registry;

		public EncounterRegister(Mod target, Registries.EncounterRegistry registry) {
			this._target = target;
			this._registry = registry;
		}

		public void Register<T>()
		where T : Encounters.Encounter, new() {
			this._registry.register<T>(this._target);
		}

	}
}
