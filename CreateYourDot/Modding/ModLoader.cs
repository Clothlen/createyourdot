namespace CreateYourDot.Modding {
	using System;
	using System.Collections.Generic;

	internal static class ModLoader
	{

		internal static readonly Dictionary<string, Encounters.Encounter> encounters_ = new Dictionary<string, Encounters.Encounter>();

		internal static Registries.EncounterRegistry encounters;

		internal static readonly Dictionary<string, Mod> mods = new Dictionary<string, Mod>();

		internal static void load_all() {
			var mods = new Mod[] {
				// Just load the example mod for now.
				new EncounterSkeleton.Mod()
			};

			foreach (var mod in mods) {
				add(mod);
			}

		}

		internal static void add(Mod mod) {
			mods.Add(mod.Id, mod);
		}

		internal static void register_encounters() {
			var encounter_register = new Registries.EncounterRegistry();

			foreach (var mod in mods.Values) {
				var register = new Registers.EncounterRegister(mod, encounter_register);
				mod.RegisterEncounters(register);
			}

			encounters = encounter_register;

		}

		internal static Scenes.Battle.Objects.Enemy get_enemy(string location) {
			var (modid, item) = split_location(location);

			return (Scenes.Battle.Objects.Enemy)Godot.ResourceLoader.Load<Godot.PackedScene>($"res://mods/{modid}/Enemies/{item}.tscn").Instance();
		}

		internal static void add_encounter(string id, Encounters.Encounter encounter) {
			encounters_.Add($"{id}:{encounter.Id}", encounter);
		}

		internal static (string modid, string location) split_location(string location) {
			// Locations are all a bit of a mess.
			var splitted = location.Split(new[] { ':' }, 2);
			if (splitted.Length != 2) {
				throw new ArgumentException("Location could not be split into two.");
			}

			return (splitted[0], splitted[1]);
		}

	}
}
