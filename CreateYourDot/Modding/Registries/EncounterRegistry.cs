namespace CreateYourDot.Modding.Registries {
	using System;
	using System.Collections.Generic;

	public sealed class EncounterRegistry : IRegistry
	{

		private readonly Dictionary<string, Dictionary<string, Type>> _encounter_types = new Dictionary<string, Dictionary<string, Type>>();

		internal Type get(string modid, string encounter_id) {
			return this._encounter_types[modid][encounter_id];
		}

		internal Encounters.Encounter create(string modid, string encounter_id) {
			var encounter = (Encounters.Encounter)Activator.CreateInstance(this.get(modid, encounter_id));
			encounter.ParentMod = ModLoader.mods[modid];
			return encounter;
		}

		internal void register<T>(Mod target)
		where T : Encounters.Encounter, new() {
			var mid = target.Id;
			if (!this._encounter_types.ContainsKey(target.Id)) {
				this._encounter_types.Add(mid, new Dictionary<string, Type>());
			}

			var type = typeof(T);
			var id = new T().Id;
			this._encounter_types[mid].Add(id, type);
		}

	}
}
