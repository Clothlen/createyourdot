namespace CreateYourDot.Scenes.Text {
	using System.Collections.Generic;
	using Newtonsoft.Json;

	[JsonObject(ItemRequired=Required.Always)]
	public sealed class ImageFontData
	{

		// Setting the values to `null!` by default
		// gets rid of warning `CS8618`. You should never
		// get a null error here as long as you deserialize
		// it properly. Do not make instances of this object.
		private ImageFontData() { }

		[JsonProperty("voice")]
		public string Voice { get; private set; } = null!;

		[JsonProperty("color")]
		public string Color { get; private set; } = null!;

		[JsonProperty("linespacing")]
		public float Linespacing { get; private set; } = 0;

		[JsonProperty("sprites")]
		public Dictionary<string, Sprite> Sprites { get; private set; } = null!;

		[JsonObject(MemberSerialization.OptOut, ItemRequired=Required.Always)]
		public class Sprite
		{
			public float x;
			public float y;
			public float w;
			public float h;

			[JsonProperty("bx", Required=Required.DisallowNull)]
			public float? bx;

			[JsonProperty("by", Required=Required.DisallowNull)]
			public float? by;

			[JsonProperty("bw", Required=Required.DisallowNull)]
			public float? bw;

			[JsonProperty("bh", Required=Required.DisallowNull)]
			public float? bh;
		}

	}
}
