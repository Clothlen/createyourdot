namespace CreateYourDot.Scenes.Text.Tokens {
	using System;

	public abstract class CommandToken : IToken
	{

		public abstract string[] Aliases { get; }

		public bool CanHandle(string str) {
			if (!(str.StartsWith("[") && str.EndsWith("]"))) {
				return false;
			}

			foreach (var alias in this.Aliases) {
				if (str.StartsWith($"[{alias}:") || str.StartsWith($"[{alias}]")) {
					return true;
				}
			}

			return false;

		}

		public System.Threading.Tasks.Task<string> Process(TextBox box, string text) {
			var splitted = text.Split(new[] { ':' }, 2);

			if (splitted.Length < 1) {
				throw new ArgumentException("Invalid command process text.");
			}

			var command = splitted[0];
			// Debug.Assert();

			string args = string.Empty;
			if (splitted.Length == 2) {
				// Remove last `]`.
				args = splitted[1].Substring(0, splitted[1].Length - 1);
			}

			return this.HandleCommand(box, args);

		}

		public abstract System.Threading.Tasks.Task<string> HandleCommand(TextBox box, string args);

	}
}
