namespace CreateYourDot.Scenes.Text.Tokens {

	using System.Collections.Generic;
	using System.Text;
	using Godot;

	public static class TokenUtil
	{

		private static readonly List<IToken> _default_tokens = new List<IToken>() {
			new Commands.Color(),
			new Commands.Wait(),
			new Commands.Speed(),
			new Commands.Instant(),
			new Commands.Font(),
			new Commands.Bracket(),
			new Commands.CharacterSpacing(),
			new Commands.LineSpacing(),
			new Commands.Magic()
		};

		public static List<string> GetTokens(string text) {
			// Return a sequence of tokens.
			bool is_command = false;
			var tokens = new List<string>();
			var current_token = new StringBuilder();
			for (int i = 0; i < text.Length; i++) {
				var current_chara = text[i];
				// GD.Print(current_chara);

				if (current_chara == '\n') {
					// Save current token, add a newline token, then start over.
					tokens.Add(current_token.ToString());
					current_token.Clear();
					current_token.Append(current_chara);
					tokens.Add(current_token.ToString());
					current_token.Clear();
					continue;
				}

				if (!is_command) {
					if (current_chara == '[') {
						// TODO: check to see if it is escaped.
						// We can do this by checking if the previous character is a
						// backslash. Of course, we'd have to check if that backslash
						// is escaped as well...

						// This signals the beginning of a command.
						is_command = true;

						// Save current token.
						tokens.Add(current_token.ToString());
						// And then erase it.
						current_token.Clear();
						// And then start over with the `[`.
						current_token.Append(current_chara);
					}
					else {
						current_token.Append(current_chara);
					}
				}
				else {
					if (current_chara == ']') {
						is_command = false;
						current_token.Append(current_chara);
						tokens.Add(current_token.ToString());
						current_token.Clear();
					}
					else {
						// write character to token.
						current_token.Append(current_chara);
					}
				}

			} //end for

			// Add final token.
			tokens.Add(current_token.ToString());

			return tokens;
		}

		public static List<IToken> ParseTokens(List<string> strings) {
			return _parse_tokens(strings, _default_tokens);
		}

		private static List<IToken> _parse_tokens(List<string> strings, List<IToken> tokens) {
			#if DEBUG
				foreach (var t in tokens) {
					if (t is StringToken) {
						GD.PushWarning("If you use a StringToken for parsing, all tokens will be string tokens!");
					}
				}
			#endif

			var final_tokens = new List<IToken>();

			foreach (var str in strings) {
				IToken? handling_token = null;
				// Manual handling.
				if (str == '\n'.ToString()) {
					handling_token = new NewlineToken();
				}
				else {
					foreach (var token in tokens) {
						if (token.CanHandle(str)) {
							handling_token = token;
							break;
						}
					}
				}

				if (handling_token == null) {
					handling_token = new StringToken();
				}
				// handling_token.Text = str;

				final_tokens.Add(handling_token);
			}

			return final_tokens;
		}

	}

}
