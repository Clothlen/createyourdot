namespace CreateYourDot.Scenes.Text.Tokens {
	using System.Threading.Tasks;

	public class StringToken : IToken
	{

		public bool CanHandle(string str) {
			return true;
		}

		public Task<string> Process(TextBox box, string text) {
			return Task.FromResult(text);
		}


	}
}
