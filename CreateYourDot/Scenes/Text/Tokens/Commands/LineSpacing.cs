namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System.Threading.Tasks;

	public class LineSpacing : CommandToken
	{

		public override string[] Aliases => new[] {
			"linespacing"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			if (args == "default") {
				box.LineSpacing = TextBox.DEFAULT_LINE_SPACING;
			}
			else {
				box.LineSpacing = float.Parse(args);
			}

			return Task.FromResult(string.Empty);
		}
	}
}
