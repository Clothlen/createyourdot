namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System.Threading.Tasks;

	public class CharacterSpacing : CommandToken
	{

		public override string[] Aliases => new[] {
			"charspacing"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			if (args == "default") {
				box.CharacterSpacing = TextBox.DEFAULT_CHARACTER_SPACING;
			}
			else {
				box.CharacterSpacing = float.Parse(args);
			}

			return Task.FromResult(string.Empty);
		}

	}
}
