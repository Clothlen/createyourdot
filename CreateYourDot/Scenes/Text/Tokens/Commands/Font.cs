namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System.Threading.Tasks;

	public class Font : CommandToken
	{

		public override string[] Aliases => new[] {
			"font"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			box.Font = FontManager.GetFont(args);

			return Task.FromResult(string.Empty);
		}

	}
}
