namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System.Threading.Tasks;

	public class Color : CommandToken
	{
		public override string[] Aliases => new[] {
			"color"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			if (args == "default") {
				box.Color = TextBox.DEFAULT_COLOR;
			}
			// TODO: add a "previous" to switch to previous color?
			// Might have to add a stack to track history.
			else {
				box.Color = new Godot.Color(args);
			}
			return Task.FromResult(string.Empty);
		}

	}
}
