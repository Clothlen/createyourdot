namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System;

	public class Wait : CommandToken
	{
		public override string[] Aliases => new[] {
			"wait",
			"w"
		};

		public override async System.Threading.Tasks.Task<string> HandleCommand(TextBox box, string args) {
			var time = float.Parse(args);
			if (time < 0) {
				throw new ArgumentException("Cannot wait for a negative amount of time.");
			}

			if (!box.Instant) {
				await CoreNode.WaitForTimer(time);
			}
			return string.Empty;
		}

	}
}
