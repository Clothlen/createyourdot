namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System.Threading.Tasks;

	public class Magic : CommandToken
	{

		public override string[] Aliases => new[] {
			"magic"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			box.LetterEffect = typeof(Effects.Letters.MagicEffect);

			return Task.FromResult(string.Empty);
		}

	}
}
