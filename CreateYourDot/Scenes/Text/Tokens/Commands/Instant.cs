namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System;
	using System.Threading.Tasks;

	public class Instant : CommandToken
	{

		public override string[] Aliases => new[] {
			"instant"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			if (args == "stop") {
				box.Instant = false;
			}
			else if (args.Length == 0 || args == "start") {
				box.Instant = true;
			}
			else {
				throw new ArgumentException("Invalid argument for Instant command.");
			}

			return Task.FromResult(string.Empty);
		}

	}
}
