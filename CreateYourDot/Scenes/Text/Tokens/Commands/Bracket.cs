namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System;
	using System.Threading.Tasks;

	public class Bracket : CommandToken
	{

		public override string[] Aliases => new[] {
			"bracket"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			string bracket;
			if (args == "left" || args == "l") {
				bracket = "[";
			}
			else if (args == "right" || args == "r") {
				bracket = "]";
			}
			else {
				throw new ArgumentException($"Invalid argument for Bracket command: {args}");
			}

			return Task.FromResult(bracket);
		}

	}
}
