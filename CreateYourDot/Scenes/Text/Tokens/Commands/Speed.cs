namespace CreateYourDot.Scenes.Text.Tokens.Commands {
	using System.Threading.Tasks;

	public class Speed : CommandToken
	{

		public override string[] Aliases => new[] {
			"speed"
		};

		public override Task<string> HandleCommand(TextBox box, string args) {
			if (args == "default") {
				box.Speed = TextBox.DEFAULT_SPEED;
			}
			else {
				var speed = float.Parse(args);
				box.Speed = speed;
			}

			return Task.FromResult(string.Empty);
		}
	}
}
