namespace CreateYourDot.Scenes.Text.Tokens {
	public class NewlineToken : IToken
	{
		public bool CanHandle(string str) {
			if (str == '\n'.ToString()) {
				return true;
			}
			return false;
		}

		public System.Threading.Tasks.Task<string> Process(TextBox box, string text) {
			box.NextLine();
			return System.Threading.Tasks.Task.FromResult(string.Empty);
		}
	}
}
