namespace CreateYourDot.Scenes.Text.Tokens {
	public interface IToken
	{

		bool CanHandle(string str);

		System.Threading.Tasks.Task<string> Process(TextBox box, string text);
	}
}
