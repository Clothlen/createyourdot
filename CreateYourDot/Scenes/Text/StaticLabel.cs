namespace CreateYourDot.Scenes.Text {

	using System.Collections.Generic;
	using Godot;

	// Essentially a much simpler textbox. Completely static.
	// No tokens.
	[Util.Loading.ScenePath("res://CreateYourDot/Scenes/Text/StaticLabel.tscn")]
	public class StaticLabel : AbstractText
	{

		public float CharacterSpacing { get; set; } = TextBox.DEFAULT_CHARACTER_SPACING;

		public float Linespacing = TextBox.DEFAULT_LINE_SPACING;

		public override void _Ready() {
			this.Font = FontManager.GetFont(this.FontName);

			this._reset_head();

			// this._next_line();
			this._write_text(this.Text);
		}

		public void Clear() {
			this._clear();
		}

		public override string Text {
			get => this._text;
			set {
				this._text = value;
				GD.Print($"TEXT: {value}");
				this._clear();
				this._write_text(this._text);
			}
		}

		public string FontName {
			get => this._font_name;
			set {
				this._font_name = value;
				this.Font = FontManager.GetFont(this._font_name);
			}
		}

		private void _clear() {
			this._reset_head();

			foreach (var sprite in this._Sprites) {
				sprite.Free();
			}
			this._Sprites.Clear();
		}

		private void _reset_head() {
			this._Head = Vector2.Zero;
			this._Head.y += this.Font.TallestCharacter;
		}

		private void _next_line() {
			this._Head = new Vector2(
				0,
				this._Head.y + this.Linespacing + this.Font.Data.Linespacing
			);
		}

		private void _write_text(string text) {
			foreach (var chara in text) {
				this._write_letter(chara);
			}
		}

		private void _write_letter(char letter) {
			this._write_letter(letter.ToString());
		}

		private void _write_letter(string letter) {
			var chara = this.Font.GetLetter(letter);
			chara.Move(this._Head);

			chara.Sprite.Modulate = new Color(this.Color.r, this.Color.g, this.Color.b, this.Color.a);

			this.AddChild(chara.Sprite);
			this._Sprites.Add(chara.Sprite);

			this._Head.x += chara.Width + this.CharacterSpacing;
		}

		[Export]
		private string _text = "STATIC LABEL TEST";
		[Export]
		private string _font_name = "uibattlesmall";

	}
}
