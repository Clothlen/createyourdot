namespace CreateYourDot.Scenes.Text {
	// TODO:
	// Use this in place of letters.
	// All this does is contain a single letter.
	// Effects effect these, not each letter.
	// The use case is for people who want to have an effect
	// effect multiple letters at the same time.
	public class TextToken
	{
		public Letter[] Letters = new Letter[0];
	}

}
