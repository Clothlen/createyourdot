namespace CreateYourDot.Scenes.Text {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using Godot;

	/// <summary>
	/// A text box.
	///
	/// This is a more complicated version of StaticLabel.
	/// </summary>
	[Util.Loading.ScenePath("res://CreateYourDot/Scenes/Text/TextBox.tscn")]
	public class TextBox : AbstractText
	{

		/// <summary>
		/// The default speed that text advances.
		/// </summary>
		public static readonly float DEFAULT_SPEED = 0.035f;

		/// <summary>
		/// The default distance between characters.
		/// </summary>
		public static readonly float DEFAULT_CHARACTER_SPACING = 3;

		/// <summary>
		/// The default distance between lines.
		/// </summary>
		public static readonly float DEFAULT_LINE_SPACING = 0;

		/// <summary>
		/// If true, the player can skip text to make them instant.
		///
		/// This will basically skip sleeping.
		/// </summary>
		/// <value>If we are currently allowing skipping or not.</value>
		public bool AllowSkipping { get; set; } = true;

		/// <summary>
		/// The speed that text advances at. This is a number in seconds.
		///
		/// Defaults to DEFAULT_SPEED.
		/// </summary>
		/// <value>The time between writing characters.</value>
		public float Speed { get; set; } = DEFAULT_SPEED;

		/// <summary>
		/// Determines if the text will sleep in between characters or not.
		/// </summary>
		/// <value>If it is instant or not.</value>
		public bool Instant { get; set; } = false;

		/// <summary>
		/// How far apart characters should be from each other.
		///
		/// Defaults to DEFAULT_CHARACTER_SPACING.
		/// </summary>
		/// <value>The distance.</value>
		public float CharacterSpacing { get; set; } = DEFAULT_CHARACTER_SPACING;

		/// <summary>
		/// How far apart lines should be from each other.
		///
		/// Defaults to DEFAULT_LINE_SPACING.
		/// </summary>
		/// <value>The distance.</value>
		public float LineSpacing { get; set; } = DEFAULT_LINE_SPACING;

		/// <summary>
		/// The effect applied to each letter.
		/// </summary>
		/// <returns>The current effect being applied to each letter.</returns>
		public Type LetterEffect { get; set; } = typeof(Effects.Letters.TwitchEffect);//typeof(Effects.NoneEffect);

		/// <summary>
		/// The width of the text box.
		///
		/// Changing this only affects future writing.
		/// </summary>
		/// <value>The width.</value>
		public float Width { get; private set; }

		/// <summary>
		/// If the text box is writing at the moment.
		/// </summary>
		/// <value>If it is writing at the moment.</value>
		public bool Writing { get; private set; }

		/// <summary>
		/// When the text finishes writing.
		/// </summary>
		public delegate void DialogueCompleteHandler();

		/// <summary>
		/// When the text finishes writing.
		/// </summary>
		public DialogueCompleteHandler OnDialogueComplete;

		/// <summary>
		/// Integers within this correspond to indexes of _Sprites.
		/// </summary>
		/// <typeparam name="int"></typeparam>
		/// <typeparam name="Effects.Letters.AbstractLetterEffect"></typeparam>
		/// <returns></returns>
		private readonly Dictionary<int, Effects.Letters.AbstractLetterEffect> _letter_effects = new Dictionary<int, Effects.Letters.AbstractLetterEffect>();

		/// <summary>
		/// Used for if set from the editor.
		/// </summary>
		[Export]
		private string? _text = null;

		public override void _Ready() {
			this._Head.y += this.Font.TallestCharacter;

			if (!(this._text is null)) {
				this._do_text(this._text);
			}

		}

		public override void _PhysicsProcess(float delta) {
			if (this._Sprites.Count < 1) {
				return;
			}

			foreach (var effect in this._letter_effects) {
				effect.Value.Update(delta);
			}

		}

		/// <summary>
		/// Remove all sprites and effects.
		///
		/// After calling this, it is okay to delete the object.
		/// </summary>
		public void Clear() {
			foreach (var sprite in this._Sprites) {
				sprite.Free();
			}
			this._Sprites.Clear();
			foreach (var effect in this._letter_effects.Values) {
				effect.Letter = null;
			}
			this._letter_effects.Clear();
		}

		public override string Text {
			get => this._text!;
			set {
				this._text = value;
				this.Clear();
				this._do_text(this._text);
			}
		}

		/// <summary>
		/// Insert a new line.
		/// </summary>
		public void NextLine() {
			this._Head = new Vector2(0, this._Head.y + this.LineSpacing + this.Font.Data.Linespacing);
		}

		public void Skip() {
			// TODO: this should reset.
			this.Instant = true;
		}

		private async void _do_text(string text) {
			this.Writing = true;
			var strings = Tokens.TokenUtil.GetTokens(text);
			var tokens = Tokens.TokenUtil.ParseTokens(strings);

			Debug.Assert(strings.Count == tokens.Count);

			for (int i = 0; i < tokens.Count; i++) {
				var token = tokens[i];
				var str = await token.Process(this, strings[i]);
				if (str != null) {
					// GD.Print(token.Text);
					if (this.Instant) {
						this._write_text_instant(str);
					}
					else {
						await this._write_text_async(str, this.Speed);
					}
				}
			}

			this.Writing = false;
			this.OnDialogueComplete?.Invoke();

		}

		private void _write_text_instant(string text) {
			foreach (var chara in text) {
				this._write_letter(chara);
			}
		}

		private async System.Threading.Tasks.Task _write_text_async(string text, float speed, bool skip_spaces = true) {
			var tree = this.GetTree();
			foreach (var chara in text) {

				if (!this.Instant) {
					// Don't wait if it is a space.
					if (chara == ' ') {
						if (!skip_spaces) {
							await this.ToSignal(tree.CreateTimer(speed), "timeout");
						}
					}
					else {
						await this.ToSignal(tree.CreateTimer(speed), "timeout");
					}
				}

				this._write_letter(chara);
			}
		}

		private void _write_letter(char letter) {
			this._write_letter(letter.ToString());
		}

		private void _write_letter(string letter) {
			var chara = this.Font.GetLetter(letter);
			chara.Move(this._Head);
			// Anchor to top-left instead of bottom-left?
			// chara.Move(this._head + new Vector2(0, font.TallestCharacter));

			chara.Sprite.Modulate = new Color(this.Color.r, this.Color.g, this.Color.b, this.Color.a);
			var effect = (Effects.Letters.AbstractLetterEffect)Activator.CreateInstance(this.LetterEffect);
			effect.TextBox = this;
			effect.Letter = chara;

			this.AddChild(chara.Sprite);
			this._letter_effects.Add(this._Sprites.Count, effect);
			this._Sprites.Add(chara.Sprite);

			this._Head.x += chara.Width + this.CharacterSpacing;

		}

	}
}
