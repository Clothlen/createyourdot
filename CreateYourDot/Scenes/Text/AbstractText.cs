namespace CreateYourDot.Scenes.Text {
	using System.Collections.Generic;

	public abstract class AbstractText : Godot.Node2D, IText
	{

		/// <summary>
		/// The default <see cref="Godot.Color"/> used by text.
		/// </summary>
		/// <returns>A color</returns>
		public static readonly Godot.Color DEFAULT_COLOR = new Godot.Color(1.0f, 1.0f, 1.0f, 1.0f);

		/// <summary>
		/// The text that a text object should display.
		/// </summary>
		/// <value>The text being used.</value>
		public virtual string Text { get; set; }

		/// <summary>
		/// The <see cref="ImageFont"/> used by the text object.
		/// </summary>
		/// <returns>The font being used.</returns>
		public virtual ImageFont Font { get; set; } = FontManager.GetFont("uidialog");

		/// <summary>
		/// The <see cref="Godot.Color"/> being used by the text object.
		///
		/// By default, it is DEFAULT_COLOR.
		/// </summary>
		/// <value>The color currently being used.</value>
		public virtual Godot.Color Color { get; set; } = DEFAULT_COLOR;

		/// <summary>
		/// The position of where the next letter is to be placed.
		/// </summary>
		protected Godot.Vector2 _Head = Godot.Vector2.Zero;

		/// <summary>
		/// A list of all the sprites generated from the <see cref="ImageFont"/>.
		/// </summary>
		/// <typeparam name="Godot.Sprite"></typeparam>
		/// <returns>Reference to the list of sprites.</returns>
		protected readonly List<Godot.Sprite> _Sprites = new List<Godot.Sprite>();

	}
}
