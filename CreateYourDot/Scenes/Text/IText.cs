namespace CreateYourDot.Scenes.Text {
	public interface IText
	{

		/// <summary>
		/// The text used.
		/// </summary>
		/// <value>The text currently being used.</value>
		string Text { get; set; }

		/// <summary>
		/// The <see cref="ImageFont"/> used.
		/// </summary>
		/// <value>The font currently being used.</value>
		ImageFont Font { get; set; }

	}
}
