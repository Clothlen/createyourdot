namespace CreateYourDot.Scenes.Text {
	using System.Collections.Generic;

	public static class FontManager
	{

		private static readonly Dictionary<string, ImageFont> _fonts = new Dictionary<string, ImageFont>();

		public static ImageFont GetFont(string name) {
			if (!_fonts.ContainsKey(name)) {
				_load_font(name);
			}

			return _fonts[name];
		}

		private static void _load_font(string name) {
			var font = new ImageFont(name);
			_fonts.Add(name, font);
		}

	}
}
