namespace CreateYourDot.Scenes.Text {
	public class Letter
	{

		/// <summary>
		/// The internal sprite used to paint the letter texture.
		/// </summary>
		public readonly Godot.Sprite Sprite;

		/// <summary>
		/// The name of the letter.
		/// </summary>
		public readonly string Name;

		/// <summary>
		/// The width of the letter.
		/// </summary>
		public readonly float Width;

		/// <summary>
		/// The height of the letter.
		/// </summary>
		public readonly float Height;

		/// <summary>
		/// The border dimensions for x.
		///
		/// Used for the
		/// </summary>
		public readonly float? BorderX;

		/// <summary>
		/// The border dimensions for y.
		/// </summary>
		public readonly float? BorderY;

		// We can calculate adjustment once, when this is constructed.
		// TODO: But would it be better to use a Godot.Vector2 instead of two floats?
		// We still construct a new Godot.Vector2 every Move.
		private readonly float _adjust_x;
		private readonly float _adjust_y;

		public Letter(string name, ImageFontData.Sprite spritedata, Godot.AtlasTexture texture) {
			this.Name = name;
			this.Width = texture.GetWidth();
			this.Height = texture.GetHeight();

			this.BorderX = spritedata.bx;
			this.BorderY = spritedata.by;

			this._adjust_x = this.Width / 2;
			// Anchor it to the bottom.
			this._adjust_y = -this.Height / 2;

			if (this.BorderX != null) {
				this._adjust_x += (float)this.BorderX;
			}
			if (this.BorderY != null) {
				this._adjust_y += (float)this.BorderY;
			}

			this.Sprite = new Godot.Sprite();
			this.Sprite.Texture = texture;

		}

		/// <summary>
		/// Move a letter to a new position.
		///
		/// It actually moves it to a slightly adjusted position compared to the inputted value.
		/// </summary>
		/// <param name="bottom"></param>
		public void Move(Godot.Vector2 bottom) {
			var adjust = new Godot.Vector2(
				this._adjust_x,
				this._adjust_y
			);

			this.Sprite.Position = bottom + adjust;
		}


	}
}
