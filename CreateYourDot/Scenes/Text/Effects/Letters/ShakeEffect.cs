namespace CreateYourDot.Scenes.Text.Effects.Letters {

	using System;
	using System.Threading.Tasks;
	using System.Diagnostics.Contracts;

	// TODO: may not be accurate.
	public class ShakeEffect : AbstractLetterEffect
	{

		private readonly float _intensity;
		private bool _skip_next_frame;
		private Godot.Vector2 _last_wig;

		// Setting this to above 1.0f makes it look very weird. Not like CYF I think.
		public ShakeEffect() : this(1.0f) { }

		public ShakeEffect(float intensity = 1.0f) {
			this._intensity = intensity != 0 ? intensity : 1.0f;
		}

		public override Task Update(float delta) {
			Contract.Assume(this.Letter != null);
			Contract.EndContractBlock();

			if (this._skip_next_frame) {
				this._skip_next_frame = false;
				// TODO: this isn't in the original but I think it should be?
				// Twitch resets the position but it didn't here.
				this.Letter.Sprite.Position -= this._last_wig;

				return Task.CompletedTask;
			}

			float random = (float)(Util.RandomUtil.Random.Next() * 2.0f * Math.PI);

			var wig = new Godot.Vector2(
				(float)Math.Sin(random) * this._intensity,
				(float)Math.Cos(random) * this._intensity
			);

			this.Letter.Sprite.Position += wig;
			this._skip_next_frame = true;
			this._last_wig = wig;

			return Task.CompletedTask;
		}

	}
}
