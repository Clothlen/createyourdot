namespace CreateYourDot.Scenes.Text.Effects.Letters {
	using System.Threading.Tasks;

	public class NoneEffect : AbstractLetterEffect
	{

		public override Task Update(float delta) {
			return Task.CompletedTask;
		}
	}
}
