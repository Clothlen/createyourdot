namespace CreateYourDot.Scenes.Text.Effects.Letters {
	using System;
	using System.Diagnostics.Contracts;
	using System.Threading.Tasks;

	// TODO: doesn't quite match up with CYF.
	// Seems to occur too often.
	public class TwitchEffect : AbstractLetterEffect
	{

		// public const float TWITCH_CHANCE = 0.001f;
		// public const float TWITCH_DISTANCE = 2;
		// public const float REVERT_TIME = 0.1f;

		private int _update_count = 0;
		private int _next_wig_in_frames = 0;
		private readonly float _intensity;
		private Godot.Vector2 _last_wig;

		private const int _MIN_WIG_FRAMES = 300;
		private const int _WIG_FRAME_VARIETY = 750;

		public TwitchEffect() : this(2.0f) { }

		public TwitchEffect(float intensity = 2.0f) : base() {
			this._intensity = intensity != 0 ? intensity : 2.0f;
			this._next_wig_in_frames = (int)(_WIG_FRAME_VARIETY * Util.RandomUtil.Random.NextDouble());
		}

		public override Task Update(float delta) {
			Contract.Assume(this.Letter != null);
			Contract.EndContractBlock();

			if (this._update_count == 0 && this._last_wig != null) {
				this.Letter.Sprite.Position -= this._last_wig;
			}
			this._update_count++;
			if (this._update_count < this._next_wig_in_frames) {
				return Task.CompletedTask;
			}
			this._update_count = 0;

			float random = (float)(Util.RandomUtil.Random.NextDouble() * 2.0f * Math.PI);
			var wig = new Godot.Vector2(
				(float)Math.Sin(random) * this._intensity,
				(float)Math.Cos(random) * this._intensity
			);
			this._next_wig_in_frames = _MIN_WIG_FRAMES + (int)(_WIG_FRAME_VARIETY * Util.RandomUtil.Random.NextDouble());

			this.Letter.Sprite.Position += wig;
			this._last_wig = wig;

			return Task.CompletedTask;

			// Alternative?:

			// if (Util.RandomUtil.Random.NextDouble() > TWITCH_CHANCE) {
			// 	return;
			// }

			// var dist = Util.RandomUtil.RandomDirectionalVector();
			// dist *= TWITCH_DISTANCE;

			// this.Letter.Sprite.Position += dist;

			// await CoreNode.WaitForTimer(REVERT_TIME);
			// After this point, this.Letter may be null if it was deleted.
			// if (this.Letter == null) {
			// 	return;
			// }

			// this.Letter.Sprite.Position -= dist;
		}
	}
}
