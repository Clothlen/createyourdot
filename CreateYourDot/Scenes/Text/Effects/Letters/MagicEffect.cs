namespace CreateYourDot.Scenes.Text.Effects.Letters {
	using System.Linq;
	using System.Diagnostics.Contracts;
	using System.Threading.Tasks;

	public class MagicEffect : AbstractLetterEffect
	{

		public override Task Update(float delta) {
			Contract.Assume(this.Letter != null);
			Contract.EndContractBlock();

			this.Letter.Sprite.Texture = this.TextBox.Font.Textures.Values.ElementAt(Util.RandomUtil.Random.Next(0, this.TextBox.Font.Textures.Count));

			return Task.CompletedTask;

		}

	}
}
