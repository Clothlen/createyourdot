namespace CreateYourDot.Scenes.Text.Effects.Letters {
	using System;
	using System.Diagnostics.Contracts;
	using System.Threading.Tasks;

	// TODO: this does not match CYF.
	public class RotatingEffect : AbstractLetterEffect
	{

		// TODO: should this be static and incremented globally?
		// It would make all the letters sync up even if some are skipped.
		private float _sin_timer;
		private readonly float _intensity;
		private const float _ROTATION_SPEED = 7.0f;

		public RotatingEffect() : this(1.5f) { }

		public RotatingEffect(float intensity = 1.5f) {
			this._intensity = intensity != 0 ? intensity : 1.5f;
		}

		public override Task Update(float delta) {
			Contract.Assume(this.Letter != null);
			Contract.EndContractBlock();

			float iDiv = this._sin_timer * _ROTATION_SPEED;
			this.Letter.Sprite.Position += new Godot.Vector2(
				// x was multiplied by -1 in CYF.
				(float)Math.Sin(iDiv) * this._intensity,
				(float)Math.Cos(iDiv) * this._intensity
			);

			// TODO: we need to get `delta` instead.
			this._sin_timer += delta;

			return Task.CompletedTask;
		}
	}
}
