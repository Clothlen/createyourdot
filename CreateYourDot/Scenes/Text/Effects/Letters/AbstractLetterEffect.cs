namespace CreateYourDot.Scenes.Text.Effects.Letters {
	using System.Threading.Tasks;

	public abstract class AbstractLetterEffect
	{

		public Letter? Letter { get; set; }

		public TextBox TextBox { get; set; }

		public abstract Task Update(float delta);
	}
}
