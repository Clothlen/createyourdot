namespace CreateYourDot.Scenes.Text {

	using System.Collections.Generic;
	using System.IO;
	using System.Diagnostics;

	public class ImageFont
	{

		// Cannot be char because some keys are not single characters.
		public readonly Dictionary<string, Godot.AtlasTexture> Textures = new Dictionary<string, Godot.AtlasTexture>();

		public float TallestCharacter { get; private set; }

		public ImageFontData Data { get; private set; }

		public ImageFont(string name) {

			const string ROOT = "res://resources/font";
			var base_path = $"{ROOT}/{name}";

			var json_path = Godot.ProjectSettings.GlobalizePath($"{base_path}.json");
			var image_path = Godot.ProjectSettings.GlobalizePath($"{base_path}.png");

			var json_text = File.ReadAllText(json_path);
			var data = Newtonsoft.Json.JsonConvert.DeserializeObject<ImageFontData>(json_text);
			this.Data = data;

			var image = new Godot.Image();
			image.LoadPngFromBuffer(File.ReadAllBytes(image_path));

			var texture = new Godot.ImageTexture();
			texture.CreateFromImage(image);

			var size = image.GetSize();

			foreach (var kv in data.Sprites) {
				var sprite_name = kv.Key;
				var sprite = kv.Value;

				var atlas_texture = new Godot.AtlasTexture() {
					Atlas = texture,
					Region = new Godot.Rect2(
						sprite.x,
						// TODO: modify JSON directly instead of dealing with this
						size.y - sprite.y - sprite.h,
						sprite.w,
						sprite.h
					)
				};

				this.Textures.Add(sprite_name, atlas_texture);
			}

			this._find_tallest_character(data.Sprites.Values);

		}

		public Letter GetLetter(string name) {
			Debug.Assert(this.Textures.ContainsKey(name), $"Letter `{name}` doesn't exist.");

			var letter_texture = this.Textures[name];
			var letter = new Letter(name, this.Data.Sprites[name], letter_texture);
			return letter;

		}

		private void _find_tallest_character(IEnumerable<ImageFontData.Sprite> sprites) {
			var tallest = float.MinValue;

			foreach (var sprite in sprites) {
				if (sprite.h > tallest) {
					tallest = sprite.h;
				}
			}

			this.TallestCharacter = tallest;

		}

	}
}
