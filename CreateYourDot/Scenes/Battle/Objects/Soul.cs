namespace CreateYourDot.Scenes.Battle.Objects {
	using Godot;

	public class Soul : KinematicBody2D
	{

		public const float BLINK_CYCLE_SECONDS = 0.18f;

		public float Speed { get; set; } = 2.0f;

		public float BlinkCycleSeconds { get; set; } = BLINK_CYCLE_SECONDS;

		protected Vector2 _MovementDirection = new Vector2(0, 0);

		public override void _PhysicsProcess(float delta) {
			// this.Position += new Vector2(this.Speed, this.Speed) * this._MovementDirection;
			this.MoveAndCollide(new Vector2(this.Speed, this.Speed) * this._MovementDirection);
		}

		public void Explode() {
			// TODO: Heart explodes into pieces
		}

		protected bool _CurrentlyBlinked {
			get => this._currently_blinked;
			set {
				this._currently_blinked = value;
				var c = this.Modulate;
				if (value) {
					c.a = 0.0f;
				}
				else {
					c.a = 1.0f;
				}
				this.Modulate = c;
			}
		}
		private bool _currently_blinked = false;

	}
}
