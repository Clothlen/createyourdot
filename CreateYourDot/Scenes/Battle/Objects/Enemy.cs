namespace CreateYourDot.Scenes.Battle.Objects {
	using System;

	public abstract class Enemy : Godot.KinematicBody2D, IEntity
	{

		public new virtual string Name { get; set; } = "";

		public virtual float Health { get; set; } = 100;

		public virtual float FakeAttack { get; set; } = 10;

		public virtual float Defense { get; set; } = 10;

		public virtual float Gold { get; set; } = 10_000;

		public virtual string CheckText { get; set; } = "The developer forgot to set their check text...";

		public virtual bool Unkillable { get; set; } = false;

		public virtual string[] Comments { get; set; } = new[] { "The developer forgot to set their comments." };

		public virtual string[] Commands { get; set; } = new string[0];

		public virtual string[] Dialogue { get; set; } = new[] { "Test." };

		public bool CanSpare { get; set; } = false;

		public bool CanCheck { get; set; } = true;

		public sealed override void _Ready() {
			// this.GetNode
		}

		public void OnTurnStart() {

		}

		public void AddWave<T>() {
			this.AddWave(typeof(T));
		}

		public void AddWave(Type type) {

		}

		// This is like HandleAttack
		public virtual void OnAttacked(float attack_status) { }

		// This is like HandleCustomCommand
		public virtual void OnCommand(string command) {


		}

	}
}
