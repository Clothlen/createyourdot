namespace CreateYourDot.Scenes.Battle {
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;

	public abstract class AbstractBattle : Godot.Node2D
	{

		public Util.Loading.ContentLoader Loader { get; protected set; } = new Util.Loading.ContentLoader();

		public Arena.IArena Arena { get; protected set; }

		public UT.Player.Player Player { get; protected set; }

		public Encounters.Encounter Encounter { get; protected set; }

		public Audio Audio { get; protected set; }

		public readonly ReadOnlyCollection<Waves.Wave> Waves;

		public readonly ReadOnlyCollection<Enemies.Enemy> Enemies;

		public readonly ReadOnlyDictionary<string, Godot.Node2D> Layers;

		protected readonly Dictionary<string, Godot.Node2D> _Layers = new Dictionary<string, Godot.Node2D>();

		protected readonly List<Waves.Wave> _Waves = new List<Waves.Wave>();

		protected readonly List<Enemies.Enemy> _Enemies = new List<Enemies.Enemy>();

		public AbstractBattle() : base() {
			this.Waves = new ReadOnlyCollection<Waves.Wave>(this._Waves);
			this.Enemies = new ReadOnlyCollection<Enemies.Enemy>(this._Enemies);
			this.Layers = new ReadOnlyDictionary<string, Godot.Node2D>(this._Layers);
		}

		public override void _EnterTree() {
			this.Audio = new Audio();

			// These indexes are not necessarily constant.
			// Do not rely on them.
			// Use GetIndex() on a Layer node instead.
			this.CreateLayer("VeryLowest", 0);
			this.CreateLayer("Bottom", 1);
			this.CreateLayer("UI", 2);
			this.CreateLayer("Arena", 3);
			this.CreateLayer("Player", 4);
			this.CreateLayer("Bullet", 5);
			this.CreateLayer("Top", 6);
			this.CreateLayer("VeryHighest", 7);
		}

		public override void _ExitTree() {
			this.Audio.Free();
			this.Loader.Unload();
		}

		public Godot.Node2D CreateLayer(string name, string position, bool below = false) {
			var parent_index = this._Layers[name].GetIndex();
			if (!below) {
				parent_index++;
			}
			return this.CreateLayer(name, parent_index);
		}

		public Godot.Node2D CreateLayer(string name, int index) {
			var layer = new Godot.Node2D();
			layer.Name = $"Layer{name}";

			this._Layers.Add(name, layer);

			this.AddChild(layer);
			this.MoveChild(layer, index);

			return layer;
		}

		public virtual void AddChildToLayer(string name, Godot.Node node, bool legible_unique_name = false) {
			this._Layers[name].AddChild(node, legible_unique_name);
		}

		public virtual void AddChildToLayerBelow(string name, Godot.Node node, Godot.Node child_node, bool legible_unique_name = false) {
			this._Layers[name].AddChildBelowNode(node, child_node, legible_unique_name);
		}

		public virtual void StartWave<T>(Modding.Mod mod)
		where T : Waves.Wave, new() {
			var wave = new T() {
				Mod = mod,
				Battle = this
			};
			this._Waves.Add(wave);
			wave.internal_create();
		}

		public virtual void StartWave(Type type, Modding.Mod mod) {
			if (!type.IsSubclassOf(typeof(Waves.Wave))) {
				throw new ArgumentException("Type isn't a Wave.");
			}

			var wave = (Waves.Wave)Activator.CreateInstance(type);
			wave.Mod = mod;
			wave.Battle = this;

			this._Waves.Add(wave);
			wave.internal_create();
		}

		public virtual void StartRandomWave(IEnumerable<Type> waves, Modding.Mod mod) {
			this.StartWave(Util.RandomUtil.Choose(waves), mod);
		}

		public void EndWave(Waves.Wave wave) {
			this._Waves.Remove(wave);
		}

		public virtual void State(string name) { }

		public virtual void Dialog(string text) { }

	}
}
