namespace CreateYourDot.Scenes.Battle.UT {
	using System;
	using System.Collections.Generic;
	using System.Collections.ObjectModel;
	using Godot;

	public sealed class Battle : AbstractBattle
	{

		public UI.UI UI { get; private set; }

		public BattleStateMachine StateMachine { get; private set; }

		public override void _EnterTree() {
			base._EnterTree();

			this.UI = this.Loader.Instance<UI.UI>();
			this.UI.Battle = this;

			// 640 == width of window
			const int WINDOW_NEGATIVE_HALF = -1 * 640 / 2;
			this.UI.Position = new Vector2(WINDOW_NEGATIVE_HALF, 10);

			this.AddChildToLayer("UI", this.UI);

			this.StateMachine = new BattleStateMachine(this);

		}

		public override void _Ready() {
			base._Ready();

			this.Audio.LoadFile(this.Audio.MainMusic);

			// Setup the player.
			this.Player = this.Loader.Instance<Player.Player>();
			this.Player.Battle = this;
			this.AddChild(this.Player);

			this.StateMachine.Start("action_select");

			// This teleports the player to the first button.
			this.UI.Buttons.Index = 0;

			// Make arena.
			this.Arena = new Arena.RectangleArena(new Vector2(165, 140), 5);
			this.Arena.Node.Position = new Vector2(0, 79);

			this.AddChild(this.Arena.Node);

			this.Arena.Active = false;


			// Just for now.
			this.Encounter = Modding.ModLoader.encounters.create("EncounterSkeleton", "SkeletonEncounter");
			this.Encounter.Battle = this;
			this.Encounter.Create();

			this._auto_position_enemies();

		}

		public override void _PhysicsProcess(float delta) {
			// foreach (var wave in this._waves) {
			// 	wave.update(delta);
			// }

			this.StateMachine.Update(delta);

			this.Encounter.Update(delta);
			foreach (var enemy in this._Enemies) {
				enemy.Update(delta);
			}

			for (int i = this._Waves.Count - 1; i >= 0; i--) {
				this._Waves[i].internal_update(delta);
			}

		}

		public override void _Input(InputEvent evt) {
			this.StateMachine.Input(evt);
		}

		public void AddEnemy<T>(Modding.Mod mod)
		where T : Enemies.Enemy, new() {
			var e = new T() {
				Battle = this,
				ParentMod = mod
			};

			var instance = (Node2D)this.Loader.Instance($"res://mods/{e.ParentMod.Id}/Enemies/{e.Id}.tscn");
			e.Node = instance;

			this._Enemies.Add(e);
			this.Layers["UI"].AddChild(e.Node);
			e.Create();
		}

		public override void State(string name) {
			if (this.StateMachine.Current != null) {
				this.StateMachine.Transfer(this.StateMachine.Current, name);
			}
			else {
				this.StateMachine.Push(name);
			}
		}

		public override void Dialog(string dialog) {
			this.StateMachine.DialogResultState.Texts = new[] { dialog };
		}

		private void _auto_position_enemies() {
			// TODO
			this._Enemies[0].Node.Position = new Vector2(0, -95);
		}

	}
}
