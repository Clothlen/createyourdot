namespace CreateYourDot.Scenes.Battle.UT.States {
	// Everything is done. The battle will end now.
	public class DoneState : AbstractBattleState
	{
		public override string Name => "done";

		public override void Enter(string? from) {
			// TODO: Just for now.
			// Later we should go to the main menu.
			this.Battle.GetTree().Quit();
		}
	}
}
