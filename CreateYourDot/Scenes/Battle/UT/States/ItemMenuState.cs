namespace CreateYourDot.Scenes.Battle.UT.States {
	using System.Diagnostics;
	using System.Collections.Generic;
	using System.Linq;

	// The player is choosing an item to use.
	public class ItemMenuState : AbstractBattleState
	{
		public override string Name => "item_menu";

		private readonly List<Text.StaticLabel> _labels = new List<Text.StaticLabel>();
		private Text.StaticLabel _page_label;

		private int _choice_x = 0;
		private int _choice_y = 0;
		private int _page = 0;
		private int _pages;
		private int[] _selection_matrix;
		private const int _SELECTION_MATRIX_WIDTH = 2;
		private const int _SELECTION_MATRIX_HEIGHT = 2;
		private const float _TOP_MARGIN = 21;
		private const float _LEFT_MARGIN = 54;
		private readonly Godot.Vector2[] _positions;

		public ItemMenuState() : base() {
			this._positions = new[] {
				new Godot.Vector2(0, 0),
				new Godot.Vector2(265, 0),
				new Godot.Vector2(0, 30),
				new Godot.Vector2(265, 30)
			};

			var margin = new Godot.Vector2(_LEFT_MARGIN, _TOP_MARGIN);
			for (int i = 0; i < this._positions.Length; i++) {
				this._positions[i] += margin;
			}
		}

		public override void Enter(string? from) {
			this._page = 0;
			this._selection_matrix = new int[_SELECTION_MATRIX_WIDTH * _SELECTION_MATRIX_HEIGHT];

			this._page_label = this.Battle.Loader.Instance<Text.StaticLabel>();
			this._page_label.FontName = "uidialog";
			this._page_label.Text = "PAGE 1";
			this.Battle.UI.UIBox.AddChild(this._page_label);
			this._page_label.Position = new Godot.Vector2(320, 100);

			// Find all items.
			var inv = this.Battle.Player.Inventory;
			if (inv.Fullness < 1) {
				this.Battle.State("action_select");
				// TODO: move soul back to the item button; by default it starts at the fight button.
				return;
			}
			this._pages = (int)System.Math.Ceiling((double)(inv.Capacity / (_SELECTION_MATRIX_WIDTH * _SELECTION_MATRIX_HEIGHT)));

			this._refresh();

			Debug.Assert(this._positions.Length == this._selection_matrix.Length);

			this._catch_player();
		}

		public override void Exit(string? to) {
			foreach (var label in this._labels) {
				label.Free();
			}
			this._page_label.Free();
			this._labels.Clear();
		}

		public override void Input(Godot.InputEvent evt) {
			if (evt is Godot.InputEventKey key) {
				if (key.IsActionPressed("ui_right")) {
					if (this._choice_x == _SELECTION_MATRIX_WIDTH - 1 || !this._item_at_matrix(this._choice_x + 1, this._choice_y)) {
						// Head to the next page on the right.

						// Reset choice x to 0.
						this._choice_x = 0;

						this._set_page(this._page + 1);

						// If there isn't an item on the bottom level, move to the top level.
						if (this._choice_y == _SELECTION_MATRIX_HEIGHT - 1 && (!this._item_at_matrix())) {
							this._choice_y--;
							// No need to wrap.

							this._catch_player();
						}
					}
					else {
						var old = this._choice_x;
						this._choice_x++;
						this._bind_choice();
						if (this._item_at_matrix()) {
							this._catch_player();
						}
						else {
							this._choice_x = old;
						}
					}
				}
				else if (key.IsActionPressed("ui_left")) {
					if (this._choice_x == 0) {
						// Head to the next page on the left.

						// Reset choice x to the other side.
						this._choice_x = _SELECTION_MATRIX_WIDTH - 1;

						this._set_page(this._page - 1);

						// Is there an item here currently?
						// (Above, we only need to check on the bottom level.)
						if (!this._item_at_matrix()) {
							this._choice_x--;
							// No need to wrap.

							this._catch_player();
							if (!this._item_at_matrix()) {
								this._choice_y--;

								this._catch_player();

								if (!this._item_at_matrix()) {
									throw new System.InvalidOperationException("Invalid state.");
								}
							}
						}
					}
					else {
						var old = this._choice_x;
						this._choice_x--;
						this._bind_choice();
						if (this._item_at_matrix()) {
							this._catch_player();
						}
						else {
							this._choice_x = old;
						}
					}
				}
				if (key.IsActionPressed("ui_up")) {
					var old = this._choice_y;
					this._choice_y++;
					this._bind_choice();
					if (this._item_at_matrix()) {
						this._catch_player();
					}
					else {
						this._choice_y = old;
					}
				}
				else if (key.IsActionPressed("ui_down")) {
					var old = this._choice_y;
					this._choice_y--;
					this._bind_choice();
					if (this._item_at_matrix()) {
						this._catch_player();
					}
					else {
						this._choice_y = old;
					}
				}
				else if (key.IsActionPressed("ui_accept")) {
					this._accept();
				}
			}
		}

		private void _accept() {
			// Use an item.
			var stack = this.Battle.Player.Inventory[this._choice_x];
			if (stack is null) {
				return;
			}
			var item = stack.Item;

			if (item is Inventory.Items.UsableItem usable) {
				var old_count = this._StateMachine.ChangeCount;
				usable.Use(this.Battle);
				// If the item hasn't changed the state, head to defending.
				if (this._StateMachine.ChangeCount == old_count) {
					this.Battle.State("defending");
				}
			}

		}

		private Inventory.ItemStack?[] _get_slice(Inventory.AbstractInventory inv, int from, int to) {
			// TODO: this is slow, inefficient.
			var result = inv.Skip(from).Take(to).ToArray();
			return result;
		}

		private void _catch_player() {
			var player = this.Battle.Player;

			if (player.GetParent() != this.Battle.UI.UIBox) {
				player.GetParent().RemoveChild(player);
				this.Battle.UI.UIBox.AddChild(player);
			}

			player.Position = this._positions[this._choice_y * _SELECTION_MATRIX_WIDTH + this._choice_x] + new Godot.Vector2(
				-21,
				18
			);

		}

		private bool _item_at_matrix() {
			return this._item_at_matrix(this._choice_x, this._choice_y);
		}

		private bool _item_at_matrix(int x, int y) {
			if (this._selection_matrix[y * _SELECTION_MATRIX_WIDTH + x] == 1) {
				return true;
			}

			return false;
		}

		private void _bind_choice() {

			if (this._choice_x >= _SELECTION_MATRIX_WIDTH) {
				this._choice_x = 0;
			}
			if (this._choice_x < 0) {
				this._choice_x = _SELECTION_MATRIX_WIDTH - 1;
			}
			if (this._choice_y >= _SELECTION_MATRIX_HEIGHT) {
				this._choice_y = 0;
			}
			if (this._choice_y < 0) {
				this._choice_y = _SELECTION_MATRIX_HEIGHT - 1;
			}
		}

		private void _set_page(int page) {
			if (page >= this._pages) {
				page = 0;
			}
			else if (page < 0) {
				page = this._pages - 1;
			}
			this._page = page;
			this._page_label.Text = $"PAGE {this._page + 1}";

			this._refresh();

		}

		private void _refresh() {
			// Remove old if existing.
			foreach (var label in this._labels) {
				label.Free();
			}
			for (int i = 0; i < this._selection_matrix.Length; i++) {
				this._selection_matrix[i] = 0;
			}
			this._labels.Clear();

			int mult = _SELECTION_MATRIX_WIDTH * _SELECTION_MATRIX_HEIGHT;
			int from = this._page * mult;
			int to = this._page * mult + mult;
			var stacks = this._get_slice(this.Battle.Player.Inventory, from, to);
			for (int i = 0; i < stacks.Length; i++) {
				var stack = stacks[i];
				if (stack is null) {
					continue;
				}
				this._selection_matrix[i] = 1;
				var item = stack.Item;

				// Display its info.
				var inst = this.Battle.Loader.Instance<Text.StaticLabel>();
				inst.FontName = "uidialog";
				this.Battle.UI.UIBox.AddChild(inst);
				this._labels.Add(inst);
				inst.Text = $"* {item.Name}";

			}

			Debug.Assert(stacks.Length <= this._positions.Length);

			for (int i = 0; i < this._labels.Count; i++) {
				this._labels[i].Position = this._positions[i];
			}
			this._catch_player();

		}

	}
}
