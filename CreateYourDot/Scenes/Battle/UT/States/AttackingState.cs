namespace CreateYourDot.Scenes.Battle.UT.States {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics.Contracts;
	using System.Threading.Tasks;
	using Godot;

	// The player is attacking an enemy.
	public class AttackingState : AbstractBattleState
	{

		public static readonly float DEFAULT_SPEED = 450.0f;

		public override string Name => "attacking";

		public float DefaultSpeed { get; set; } = DEFAULT_SPEED;

		public float Speed { get; set; } = DEFAULT_SPEED;//1;//8;

		public bool Attacking { get; private set; } = false;

		public static int AttackChoice { get; internal set; } = 0;

		private Node2D? _instance;
		private AnimatedSprite _flasher;
		private AnimatedSprite _slicer;
		private Sprite _sprite;
		private Vector2 _sprite_size;
		private ColorRect _health_bar_full;
		private ColorRect _health_bar_curr;

		public override void Enter(string? from) {
			this.Battle.UI.UIBox.TextBox.Clear();

			// Spawn the menu.
			this._instance = (Node2D)this.Battle.Loader.Instance("res://CreateYourDot/Scenes/Battle/UT/UI/Menus/FightMenu.tscn");

			this._sprite = this._instance.GetNode<Sprite>("Eye");
			this._sprite_size = this._sprite.Texture.GetSize();

			this._instance.Position = new Vector2(this.Battle.UI.UIBox.Width / 2 + 32, this.Battle.UI.UIBox.Height / 2);

			this._flasher = this._instance.GetNode<AnimatedSprite>("Flasher");
			this._flasher.Position = new Vector2(this.Battle.UI.UIBox.Width / 2, 0);

			this._slicer = this._instance.GetNode<AnimatedSprite>("Slicer");
			this._slicer.Hide();

			float health_bar_width = 50;
			this._health_bar_full = new ColorRect() {
				Color = new Color(64 / 255, 64 / 255, 64 / 255),
				RectSize = new Vector2(health_bar_width, 10),
				RectPosition = new Vector2(-health_bar_width / 2, 0)
			};

			this._health_bar_curr = new ColorRect() {
				Color = new Color(0.0f, 1.0f, 0.0f),
				RectSize = new Vector2(health_bar_width, 10)
			};

			this._health_bar_full.Hide();
			// this._health_bar_curr.Hide();
			this._instance.AddChild(this._health_bar_full);
			this._health_bar_full.AddChild(this._health_bar_curr);

			this.Battle.UI.AddChild(this._instance);
			this.Battle.Player.Hide();
		}

		public override void Exit(string? to) {
			Contract.Assume(this._instance != null);

			this.Battle.UI.RemoveChild(this._instance);
			this._instance.QueueFree();
			this._instance = null;
			this.Battle.Player.Show();

			this.Speed = this.DefaultSpeed;
			this.Attacking = false;
		}

		public override void Update(float delta) {
			this._flasher.Position += new Vector2(-this.Speed * delta, 0);
			if (this._flasher.Position.x < -this.Battle.UI.UIBox.Width / 2) {
				// We're done and the player did not attack.
				this._skip_attack();
			}

		}

		public override void Input(InputEvent evt) {
			if (evt is InputEventKey key) {
				if (key.IsActionPressed("ui_accept")) {

					if (!this.Attacking) {
						this.Attacking = true;
						this._attack();
					}
				}
			}
		}

		private async void _attack() {
			Contract.Assume(this._instance != null);

			var enemy = this.Battle.Enemies[AttackChoice];

			this.Speed = 0;
			this._flasher.Play("flashing");
			// Where did they attack?
			// https://github.com/RhenaudTheLukark/CreateYourFrisk/blob/master/Assets/Scripts/Battle/FightUIController.cs#L148
			// FightUIController > getAtkMult
			const float CENTER_SIZE = 12.0f;
			const float MAXIMUM_MULT = 2.2f;

			float position_multiplier;
			if (Math.Abs(this._flasher.Position.x) <= CENTER_SIZE) {
				position_multiplier = MAXIMUM_MULT;
			}
			else {
				float mult = 2.0f - 2.0f * Math.Abs(this._flasher.Position.x * 2.0f / this._sprite_size.x);
				// Prevent negative values.
				if (mult < 0) {
					mult = 0;
				}

				position_multiplier = mult;
			}

			// Reparent it to the enemy to center it.
			this._instance.RemoveChild(this._slicer);
			enemy.Node.AddChild(this._slicer);

			this._slicer.Show();
			this._slicer.Play("slice");
			this.Battle.Audio.PlaySound("res://resources/audio/slice.wav");

			await this._slicer.ToSignal(this._slicer, "animation_finished");
			enemy.Node.RemoveChild(this._slicer);
			this._instance.AddChild(this._slicer);

			this._slicer.Hide();

			// Then, we show healthbar and damage.
			this._health_bar_full.GetParent().RemoveChild(this._health_bar_full);
			enemy.Node.AddChild(this._health_bar_full);
			this._health_bar_full.Show();
			this._health_bar_curr.RectSize = new Vector2(enemy.Health / enemy.MaxHealth * this._health_bar_full.RectSize.x, this._health_bar_curr.RectSize.y);

			// Also play hit sound here.
			this.Battle.Audio.PlaySound("res://resources/audio/hitsound.wav");

			await this._animate_enemy_hit(enemy.Node);

			this._health_bar_full.Hide();
			this._health_bar_full.GetParent().RemoveChild(this._health_bar_full);
			this._instance.AddChild(this._health_bar_full);
			this._health_bar_curr.RectSize = this._health_bar_full.RectSize;

			var final_attack = this.Battle.Player.Weapon.Item.Attack + this.Battle.Player.Attack - enemy.Defense + (GD.Randf() * 2.0f);
			final_attack *= position_multiplier;
			final_attack = MathF.Round(final_attack);

			enemy.Health -= final_attack;
			enemy.OnAttacked(final_attack);
			if (enemy.Health <= 0.0f) {
				enemy.OnDeath();
			}

			// Switch to next turn.
			this.Transfer("enemy_dialogue");

		}

		// Invoked when the player enters this state but doesn't actually attack.
		private void _skip_attack() {
			var enemy = this.Battle.Enemies[AttackChoice];
			// TODO: perhaps we should pass null? This would deviate from the CYF api.
			enemy.OnAttacked(-1);


			this.Transfer("enemy_dialogue");
		}

		// Return is discarded.
		private async Task _animate_enemy_hit(Node2D enemy_node) {
			// TODO: the original sways back and forth, multiple times, but with decreasing intensity.

			enemy_node.Position += new Vector2(10f, 0);

			await CoreNode.WaitForTimer(0.12f);

			enemy_node.Position += new Vector2(-20f, 0);

			await CoreNode.WaitForTimer(0.12f);

			enemy_node.Position += new Vector2(10f, 0);

			await CoreNode.WaitForTimer(1.0f);


		}

	}
}
