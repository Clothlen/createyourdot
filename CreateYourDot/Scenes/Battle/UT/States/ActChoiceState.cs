namespace CreateYourDot.Scenes.Battle.UT.States {
	using System.Collections.Generic;

	public class ActChoiceState : AbstractBattleState
	{

		public override string Name => "act_choice";

		public Enemies.Enemy Enemy { get; set; }

		private readonly List<Text.StaticLabel> _labels = new List<Text.StaticLabel>();
		private int _choice;
		private string[] _choices;

		private static readonly Godot.Vector2 _MARGIN = new Godot.Vector2(70, 17);
		private static readonly float _DIVIDER = 30;

		public override void Enter(string? from) {
			var commands = this.Enemy.Commands;
			this._choice = 0;
			this._choices = commands;

			if (commands.Length < 1) {
				// Just leave.
				this.Transfer("act_menu");
				return;
			}

			for (int i = 0; i < commands.Length; i++) {
				var command = commands[i];
				var label = this.Battle.Loader.Instance<Text.StaticLabel>();
				label.FontName = "uidialog";
				label.Text = $"* {command}";

				this.Battle.UI.UIBox.AddChild(label);
				this._labels.Add(label);

				label.Position = new Godot.Vector2(0, _DIVIDER * i);
			}

			foreach (var label in this._labels) {
				label.Position += _MARGIN;
			}

			this._catch_player();

		}

		public override void Exit(string? to) {
			foreach (var label in this._labels) {
				label.Free();
			}
			this._labels.Clear();
			this._choices = new string[0];
		}

		public override void Input(Godot.InputEvent evt) {
			if (evt is Godot.InputEventKey key) {
				if (key.IsActionPressed("ui_up")) {
					this._up();
				}
				else if (key.IsActionPressed("ui_down")) {
					this._down();
				}
				else if (key.IsActionPressed("ui_accept")) {
					this._accept();
				}
			}
		}

		private void _up() {
			this._choice--;
			if (this._choice < 0) {
				this._choice = this._choices.Length - 1;
			}

			this._catch_player();
		}

		private void _down() {
			this._choice++;
			if (this._choice >= this._choices.Length) {
				this._choice = 0;
			}

			this._catch_player();
		}

		private void _accept() {
			var choice = this._choices[this._choice];
			this.Enemy.OnCommand(choice);
			this.Transfer("dialog_result");
		}

		private void _catch_player() {
			var player = this.Battle.Player;

			if (player.GetParent() != this.Battle.UI.UIBox) {
				player.GetParent().RemoveChild(player);
				this.Battle.UI.UIBox.AddChild(player);
			}

			var pos = new Godot.Vector2(0, _DIVIDER * this._choice);

			pos += _MARGIN;
			// Good enough.
			pos += new Godot.Vector2(-25, 17);

			player.Position = pos;

		}

	}
}
