namespace CreateYourDot.Scenes.Battle.UT.States {
	public abstract class AbstractBattleState : Util.States.AbstractState
	{

		public Battle Battle { get; internal set; }

	}
}
