namespace CreateYourDot.Scenes.Battle.UT.States {
	public class MercyMenuState : AbstractBattleState
	{

		public override string Name => "mercy_menu";

		private Text.StaticLabel _spare_label;
		private Text.StaticLabel _flee_label;

		private int _choice = 0;
		private const float _TOP_MARGIN = 20;
		private const float _LEFT_MARGIN = 20;
		private readonly Godot.Vector2[] _positions;

		public MercyMenuState() : base() {
			this._positions = new[] {
				new Godot.Vector2(0, 0),
				new Godot.Vector2(0, 50)
			};

			var margin = new Godot.Vector2(_TOP_MARGIN, _LEFT_MARGIN);
			for (int i = 0; i < this._positions.Length; i++) {
				this._positions[i] += margin;
			}
		}

		public override void Enter(string? from) {
			// We have two options: Spare and Flee.
			// Spare can sometimes be yellow, and Flee can
			// sometimes not be visible.
			this._spare_label = this.Battle.Loader.Instance<Text.StaticLabel>();
			this._flee_label = this.Battle.Loader.Instance<Text.StaticLabel>();

			this._spare_label.FontName = "uidialog";
			this._flee_label.FontName = "uidialog";

			this._spare_label.Text = "Spare";
			this._flee_label.Text = "Flee";

			foreach (var enemy in this.Battle.Enemies) {
				if (enemy.CanSpare) {
					this._spare_label.Modulate = new Godot.Color(255, 255, 0);
				}
			}

			this.Battle.UI.UIBox.AddChild(this._spare_label);
			this.Battle.UI.UIBox.AddChild(this._flee_label);

			this._spare_label.Position = this._positions[0];
			this._flee_label.Position = this._positions[1];

			this._catch_player();

		}

		public override void Exit(string? to) {
			this._spare_label.Free();
			this._flee_label.Free();
		}

		public override void Input(Godot.InputEvent evt) {
			if (evt is Godot.InputEventKey key) {
				if (key.IsActionPressed("ui_up")) {
					// Since there are only 2 choices, we can just hardcode all of this.
					// TODO: Not very fun for modders who want to modify this though.
					if (this._choice == 0) {
						this._choice = 1;
					}
					else {
						this._choice = 0;
					}
					this._catch_player();
				}
				else if (key.IsActionPressed("ui_down")) {
					if (this._choice == 1) {
						this._choice = 0;
					}
					else {
						this._choice = 1;
					}
					this._catch_player();
				}
				else if (key.IsActionPressed("ui_accept")) {
					this._accept();
				}
			}
		}

		private void _catch_player() {
			var player = this.Battle.Player;

			if (player.GetParent() != this.Battle.UI.UIBox) {
				player.GetParent().RemoveChild(player);
				this.Battle.UI.UIBox.AddChild(player);
			}

			player.Position = this._positions[this._choice];

		}

		private void _accept() {
			// Yeah I know this is terrible.
			// but it'll stay this way until somebody complains.
			switch (this._choice) {
				case 0: // spare
					this._spare();
					break;
				case 1: // flee
					this._flee();
					break;
				default:
					break;
			}
		}

		private void _spare() {
			var old = this._StateMachine.ChangeCount;
			foreach (var enemy in this.Battle.Enemies) {
				if (enemy.CanSpare) {
					enemy.OnSpare();
				}
			}

			if (this._StateMachine.ChangeCount == old) {
				this.Transfer("enemy_dialogue");
			}
		}

		private async void _flee() {
			// Just quit the game for now.
			// var label = this.Battle.Loader.Instance<Text.StaticLabel>("res://CreateYourDot/Scenes/Text/StaticLabel.tscn");
			var label = this.Battle.Loader.Instance<Text.StaticLabel>();
			label.FontName = "uidialog";
			this.Battle.AddChild(label);
			label.Text = "Bye";
			label.Position = new Godot.Vector2(0, 40);
			await CoreNode.WaitForTimer(1.0f);
			this.Battle.GetTree().Quit();

		}

	}
}
