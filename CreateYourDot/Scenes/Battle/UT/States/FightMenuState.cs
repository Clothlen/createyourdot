namespace CreateYourDot.Scenes.Battle.UT.States {
	using System;
	using System.Collections.Generic;
	using Godot;

	// The player is choosing an item to use.
	public class FightMenuState : AbstractBattleState
	{

		public override string Name => "fight_menu";

		private int _choice = 0;

		private List<Text.StaticLabel> _labels = new List<Text.StaticLabel>();

		public override void Enter(string? from) {

			// List all the enemies.

			foreach (var enemy in this.Battle.Enemies) {
				var inst = this.Battle.Loader.Instance<Text.StaticLabel>();
				inst.FontName = "uidialog";
				this.Battle.UI.UIBox.AddChild(inst);
				this._labels.Add(inst);
				//inst.Position = new Vector2(10 * 2 - 200, 0);
				inst.Position = new Vector2(10 * 2, 0);
				inst.Text = $"* {enemy.Name}";
			}

			this.Battle.Player.Position = new Vector2(-200, 0);

		}

		public override void Exit(string? to) {
			foreach (var label in this._labels) {
				label.Free();
			}
			this._labels.Clear();
		}

		public override void Input(InputEvent evt) {
			if (evt is InputEventKey key) {
				// TODO
				if (key.IsActionPressed("ui_up")) {

				}
				else if (key.IsActionPressed("ui_down")) {

				}
				else if (key.IsActionPressed("ui_accept")) {
					this._accept();
				}
			}
		}

		private void _accept() {
			AttackingState.AttackChoice = this._choice;
			this.Transfer("attacking");
		}

	}
}
