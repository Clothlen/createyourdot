namespace CreateYourDot.Scenes.Battle.UT.States {
	// Nothing happens in this state. At all.
	public class NoneState : AbstractBattleState
	{
		public override string Name => "none";
	}
}
