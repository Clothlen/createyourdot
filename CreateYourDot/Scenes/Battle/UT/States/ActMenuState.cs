namespace CreateYourDot.Scenes.Battle.UT.States {
	using System.Collections.Generic;

	// The player is viewing their act menu.
	public class ActMenuState : AbstractBattleState
	{

		public override string Name => "act_menu";

		private readonly List<Text.StaticLabel> _labels = new List<Text.StaticLabel>();
		private readonly List<Enemies.Enemy> _enemy_choices = new List<Enemies.Enemy>();

		private int _choice;
		private int _choices;

		private static readonly Godot.Vector2 _MARGIN = new Godot.Vector2(70, 17);
		private static readonly float _DIVIDER = 30;

		public override void Enter(string? from) {
			this._choices = this.Battle.Enemies.Count;

			// First, show the enemies.
			for (int i = 0; i < this._choices; i++) {
				var enemy = this.Battle.Enemies[i];
				var label = this.Battle.Loader.Instance<Text.StaticLabel>();
				label.FontName = "uidialog";
				label.Text = $"* {enemy.Name}";

				this.Battle.UI.UIBox.AddChild(label);
				this._labels.Add(label);
				this._enemy_choices.Add(enemy);

				label.Position = new Godot.Vector2(0, _DIVIDER * i);

				if (enemy.CanSpare) {
					label.Modulate = new Godot.Color(255, 255, 0);
				}

			}

			foreach (var label in this._labels) {
				label.Position += _MARGIN;
			}

			this._catch_player();

		}

		public override void Exit(string? to) {
			foreach (var label in this._labels) {
				label.Free();
			}
			this._labels.Clear();
			this._enemy_choices.Clear();
		}

		public override void Input(Godot.InputEvent evt) {
			if (evt is Godot.InputEventKey key) {
				if (key.IsActionPressed("ui_up")) {
					this._up();
				}
				else if (key.IsActionPressed("ui_down")) {
					this._down();
				}
				else if (key.IsActionPressed("ui_accept")) {
					this._accept();
				}
			}
		}

		private void _up() {
			this._choice--;
			if (this._choice < 0) {
				this._choice = this._choices - 1;
			}

			this._catch_player();

		}

		private void _down() {
			this._choice++;
			if (this._choice >= this._choices) {
				this._choice = 0;
			}

			this._catch_player();

		}

		private void _accept() {
			var enemy_choice = this._enemy_choices[this._choice];
			this.Battle.StateMachine.ActChoiceState.Enemy = enemy_choice;
			this.Transfer("act_choice");
		}

		private void _catch_player() {
			var player = this.Battle.Player;

			if (player.GetParent() != this.Battle.UI.UIBox) {
				player.GetParent().RemoveChild(player);
				this.Battle.UI.UIBox.AddChild(player);
			}

			var pos = new Godot.Vector2(0, _DIVIDER * this._choice);
			pos += _MARGIN;
			// Good enough.
			pos += new Godot.Vector2(-25, 17);

			player.Position = pos;

		}

	}
}
