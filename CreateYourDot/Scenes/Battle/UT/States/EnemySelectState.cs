namespace CreateYourDot.Scenes.Battle.UT.States {
	// The player is choosing somebody to fight or act on.
	public class EnemySelectState : AbstractBattleState
	{
		public override string Name => "enemy_select";
	}
}
