namespace CreateYourDot.Scenes.Battle.UT.States {

	using System.Collections.Generic;
	using Godot;

	// Show dialogue for all enemies.
	public class EnemyDialogueState : AbstractBattleState
	{

		public override string Name => "enemy_dialogue";

		private readonly List<Text.TextBox> _text_boxes = new List<Text.TextBox>();

		public override void Enter(string? from) {
			foreach (var enemy in this.Battle.Enemies) {
				var textbox = this.Battle.Loader.Instance<Text.TextBox>();
				this.Battle.AddChild(textbox);
				// TODO
				textbox.Text = string.Join('\n', enemy.Dialogue);
				this._text_boxes.Add(textbox);

			}
		}

		public override void Exit(string? to) {
			foreach (var box in this._text_boxes) {
				box.Clear();
				this.Battle.RemoveChild(box);
				box.QueueFree();
			}
			this._text_boxes.Clear();
		}

		public override void Input(InputEvent evt) {
			if (evt is InputEventKey key) {
				if (key.IsAction("skip_dialogue")) {
					this._skip();
				}
				else if (key.IsAction("ui_accept")) {
					if (this._done_writing()) {
						this.Transfer("defending");
					}
				}
			}
		}

		private void _skip() {
			foreach (var box in this._text_boxes) {
				if (box.AllowSkipping) {
					box.Skip();
				}
			}
		}

		private bool _done_writing() {
			foreach (var box in this._text_boxes) {
				if (box.Writing) {
					return false;
				}
			}

			return true;
		}

	}
}
