namespace CreateYourDot.Scenes.Battle.UT.States {
	public class ActionSelectState : AbstractBattleState
	{

		public override string Name => "action_select";

		public override void Enter(string? from) {
			this.Battle.UI.Buttons.Sleeping = false;
			this.Battle.UI.Buttons.Index = 0;
		}

		public override void Exit(string? to) {
			this.Battle.UI.UIBox.TextBox.Clear();
			this.Battle.UI.Buttons.Sleeping = true;
		}

	}
}
