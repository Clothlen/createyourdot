namespace CreateYourDot.Scenes.Battle.UT.States {
	public class DialogResultState : AbstractBattleState
	{

		public override string Name => "dialog_result";

		public string[] Texts = new[] { "No text... :(" };

		public override void Enter(string? from) {
			this.Battle.UI.UIBox.TextBox.Text = this.Texts[0];
		}

		public override void Exit(string? to) {
			this.Battle.UI.UIBox.TextBox.Text = "";
		}

		public override void Input(Godot.InputEvent evt) {
			if (evt is Godot.InputEventKey key) {
				if (key.IsActionPressed("ui_accept")) {
					if (!this.Battle.UI.UIBox.TextBox.Writing) {
						this.Transfer("enemy_dialogue");
					}
				}
			}
		}

	}
}
