namespace CreateYourDot.Scenes.Battle.UT.States {
	// The player is defending themselves from an attack.
	public class DefendingState : AbstractBattleState
	{
		public override string Name => "defending";

		public override void Enter(string? from) {
			Godot.GD.Print("Entering defending");
			this.Battle.UI.UIBox.Hide();
			this.Battle.Arena.Active = true;
			var player = this.Battle.Player;
			player.GetParent().RemoveChild(player);
			this.Battle.AddChild(player);
			player.Position = this.Battle.Arena.Node.Position;
			player.InputEnabled = true;

			foreach (var enemy in this.Battle.Enemies) {
				enemy.OnAttack();
			}
		}

		public override void Update(float delta) {
			if (this.Battle.Waves.Count <= 0) {
				this.Transfer("action_select");
			}
		}

		public override void Exit(string? to) {
			this.Battle.Player.InputEnabled = false;
			this.Battle.Arena.Active = false;
			this.Battle.UI.UIBox.Show();
		}

	}
}
