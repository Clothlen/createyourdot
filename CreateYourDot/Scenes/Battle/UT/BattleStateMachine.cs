namespace CreateYourDot.Scenes.Battle.UT {
	using States;

	public sealed class BattleStateMachine : Util.States.StateMachine
	{

		public readonly ActChoiceState ActChoiceState;

		public readonly ActionSelectState ActionSelectState;

		public readonly ActMenuState ActMenuState;

		public readonly AttackingState AttackingState;

		public readonly DefendingState DefendingState;

		public readonly DialogResultState DialogResultState;

		public readonly DoneState DoneState;

		public readonly EnemyDialogueState EnemyDialogueState;

		public readonly FightMenuState FightMenuState;

		public readonly ItemMenuState ItemMenuState;

		public readonly MercyMenuState MercyMenuState;

		public readonly NoneState NoneState;

		public BattleStateMachine(Battle battle) {
			this.ActChoiceState = new ActChoiceState();
			this.ActionSelectState = new ActionSelectState();
			this.AttackingState = new AttackingState();
			this.DefendingState = new DefendingState();
			this.DialogResultState = new DialogResultState();
			this.DoneState = new DoneState();
			this.EnemyDialogueState = new EnemyDialogueState();
			// this.EnemySelectState = new EnemySelectState();
			this.FightMenuState = new FightMenuState();
			this.ActMenuState = new ActMenuState();
			this.ItemMenuState = new ItemMenuState();
			this.MercyMenuState = new MercyMenuState();
			this.NoneState = new NoneState();


			var states = new AbstractBattleState[] {
				this.ActChoiceState,
				this.ActionSelectState,
				this.AttackingState,
				this.DefendingState,
				this.DialogResultState,
				this.DoneState,
				this.EnemyDialogueState,
				// this.EnemySelectState,
				this.FightMenuState,
				this.ActMenuState,
				this.ItemMenuState,
				this.MercyMenuState,
				this.NoneState
			};
			foreach (var s in states) {
				s.Battle = battle;
			}
			this.Add(states);

		}

	}
}
