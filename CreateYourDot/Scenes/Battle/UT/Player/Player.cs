namespace CreateYourDot.Scenes.Battle.UT.Player {
	using Godot;

	[Util.Loading.ScenePath("res://CreateYourDot/Scenes/Battle/UT/Player/Player.tscn")]
	public class Player : Objects.Soul, Projectiles.IDamageable
	{

		public Battle Battle { get; set; }

		public float Health { get; private set; } = 20;

		// increases by 2 every level, starts at 10 at level 1.
		public float Attack { get; private set; } = 10;

		public float InvulnerabilityTimer { get; set; } = 1.0f;

		public float InvulnerabilityTimeLeft { get; set; } = 0.0f;

		public Inventory.AbstractInventory Inventory { get; set; }

		public Inventory.ItemStack<Inventory.Items.AbstractWeapon> Weapon { get; set; } = new Inventory.ItemStack<Inventory.Items.AbstractWeapon>(new Inventory.Items.Weapons.ToyKnife());

		public override void _Ready() {
			base._Ready();
			this.Inventory = new PlayerInventory();
			this.SetProcessInput(false);
		}

		public override void _PhysicsProcess(float delta) {

			if (this.IsProcessingInput()) {
				if (Input.IsActionPressed("player_up")) {
					this._MovementDirection.y = -1;
				}
				else if (Input.IsActionPressed("player_down")) {
					this._MovementDirection.y = 1;
				}
				else {
					this._MovementDirection.y = 0;
				}

				if (Input.IsActionPressed("player_right")) {
					this._MovementDirection.x = 1;
				}
				else if (Input.IsActionPressed("player_left")) {
					this._MovementDirection.x = -1;
				}
				else {
					this._MovementDirection.x = 0;
				}
			}

			if (this.InvulnerabilityTimeLeft > 0.0f) {
				this.InvulnerabilityTimeLeft -= delta;

				if (this.InvulnerabilityTimeLeft <= 0.0f) {
					this._CurrentlyBlinked = false;
				}
				else {
					// this._CurrentlyBlinked = !(this.InvulnerabilityTimeLeft % this.BlinkCycleSeconds > this.BlinkCycleSeconds / 2.0f);
					this._CurrentlyBlinked = this.InvulnerabilityTimeLeft % this.BlinkCycleSeconds <= this.BlinkCycleSeconds / 2.0f;
				}
			}

			base._PhysicsProcess(delta);

		}

		public bool InputEnabled {
			get { return this.IsProcessingInput(); }
			set {
				this.SetProcessInput(value);

				if (!value) {
					// Stop moving.
					this._MovementDirection = Vector2.Zero;
				}
			}
		}

		public void Damage(float damage) {
			if (this.InvulnerabilityTimeLeft > 0.0f) {
				return;
			}

			this.Health -= damage;

			if (this.Health <= 0) {
				// Die
				this.Battle.UI.PlayerInfo.Health = 0;
				this.Explode();
				this.Battle.State("done");
			}
			else {
				this.Battle.UI.PlayerInfo.Health = this.Health;
				this.InvulnerabilityTimeLeft = this.InvulnerabilityTimer;
			}
		}

		// This is temporary until we separate Player from Node.
		public Vector2 RPosition {
			get {
				return new Vector2(
					this.GlobalPosition.x - this.Battle.Arena.Node.GlobalPosition.x,

					-(this.GlobalPosition.y - this.Battle.Arena.Node.GlobalPosition.y)
				);
			}
			set {
				this.GlobalPosition = this.Battle.Arena.Node.GlobalPosition + value;
			}
		}

	}
}
