namespace CreateYourDot.Scenes.Battle.UT.UI {
	using Godot;

	public class PlayerInfo : Node2D
	{

		public Battle Battle { get; internal set; }

		[Export]
		private string _player_name = "CLOTH";

		[Export]
		private float _health = 20;

		[Export]
		private float _max_health = 20;

		[Export]
		private int _level = 1;

		private Text.StaticLabel _player_name_label;
		private Text.StaticLabel _level_label;
		private Text.StaticLabel _hp_text;
		private ColorRect _hp_background;
		private ColorRect _hp_current;
		private Text.StaticLabel _hp_values;

		public override void _Ready() {
			this._player_name_label = this.GetNode<Text.StaticLabel>("PlayerName");
			this._level_label = this.GetNode<Text.StaticLabel>("Level");
			this._hp_text = this.GetNode<Text.StaticLabel>("HPText");
			this._hp_background = this.GetNode<ColorRect>("HPBackground");
			this._hp_current = this.GetNode<ColorRect>("HPCurrent");
			this._hp_values = this.GetNode<Text.StaticLabel>("HPValues");
		}

		public string PlayerName {
			get => this._player_name;
			set {
				this._player_name = value;
				this._player_name_label.Text = value;
			}
		}

		public float Health {
			get => this._health;
			set {
				this._health = value;
				this._hp_values.Text = $"{this._health} / {this._max_health}";
			}
		}

		public float MaxHealth {
			get => this._max_health;
			set {
				this._max_health = value;
				this._hp_values.Text = $"{this._health} / {this._max_health}";
			}
		}

		public int Level {
			get => this._level;
			set {
				this._level = value;
				this._level_label.Text = $"LV {this._level}";
			}
		}

	}
}
