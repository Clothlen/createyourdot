namespace CreateYourDot.Scenes.Battle.UT.UI {
	using Godot;

	[Util.Loading.ScenePath("res://CreateYourDot/Scenes/Battle/UT/UI/UI.tscn")]
	public class UI : Node2D
	{

		public Battle Battle { get; internal set; }

		public Buttons Buttons { get; private set; }

		public PlayerInfo PlayerInfo { get; private set; }

		public UIBox UIBox { get; private set; }

		public override void _EnterTree() {
			this.Buttons = this.GetNode<Buttons>("Buttons");
			this.Buttons.Battle = this.Battle;

			this.PlayerInfo = this.GetNode<PlayerInfo>("PlayerInfo");
			this.PlayerInfo.Battle = this.Battle;

			this.UIBox = this.GetNode<UIBox>("UIBox");
		}

		public override void _Ready() {
			this.Buttons = this.GetNode<Buttons>("Buttons");
			this.Buttons.OnButtonPress += this._on_primary_buttons_press;
		}

		private void _on_primary_buttons_press(string name) {
			GD.Print(name);
		}

	}
}
