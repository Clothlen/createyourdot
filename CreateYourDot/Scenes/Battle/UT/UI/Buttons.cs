namespace CreateYourDot.Scenes.Battle.UT.UI {
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using Godot;

	public class Buttons : Node2D
	{

		public Battle Battle { get; internal set; }

		private UIButton[] _buttons;
		private int _button_index = 0;

		public override void _EnterTree() {
			var bs = new List<UIButton>();
			foreach (var child in this.GetChildren()) {
				if (child is UIButton button) {
					button.Battle = this.Battle;
					bs.Add(button);
					button.OnPress += this._on_button_press;
				}
			}
			this._buttons = bs.ToArray();
			bs.Clear();
		}

		public override void _Ready() {
			if (this._buttons.Length < 1) {
				throw new ArgumentException("Button container has no buttons.");
			}

		}

		public override void _Input(InputEvent evt) {
			if (evt is InputEventKey key) {
				if (key.IsActionPressed("ui_left")) {
					this._previous_button();
				}
				else if (key.IsActionPressed("ui_right")) {
					this._next_button();
				}
				else if (key.IsActionPressed("ui_accept")) {
					// This stops states from handling the input
					// immediately upon entering.
					this.GetTree().SetInputAsHandled();
					this._press_button();
				}
			}
		}

		public int Index {
			get => this._button_index;
			set {
				this._button_index = value;
				this._catch_player();
			}
		}

		public bool Sleeping {
			get => this.IsProcessingInput();
			set {
				var awake = !value;
				this.SetProcessInput(awake);
				if (!awake) {
					this._deactivate_button();
				}
				else {
					this._button_index = 0;
					this._activate_button();
				}
			}
		}

		private void _previous_button() {
			this._deactivate_button();
			this.Battle.Audio.PlaySound("res://resources/audio/menumove.wav");
			this._button_index--;
			this._wrap_button_index();
			this._activate_button();

			this._catch_player();
		}

		private void _next_button() {
			this._deactivate_button();
			this.Battle.Audio.PlaySound("res://resources/audio/menumove.wav");
			this._button_index++;
			this._wrap_button_index();
			this._activate_button();

			this._catch_player();
		}

		private void _press_button() {
			this._deactivate_button();
			this.Battle.Audio.PlaySound("res://resources/audio/menuconfirm.wav");
			this._buttons[this._button_index].Press();
		}

		private void _wrap_button_index() {
			if (this._button_index < 0) {
				this._button_index = this._buttons.Length - 1;
			}
			else if (this._button_index > this._buttons.Length - 1) {
				this._button_index = 0;
			}
		}

		private void _deactivate_button() {
			this._buttons[this._button_index].Deselect();
		}

		private void _activate_button() {
			this._buttons[this._button_index].Select();
		}

		private void _catch_player() {
			var button = this._buttons[this._button_index];

			// var position = this.GetRelativeTransformToParent(this.GetParent()).origin;
			// position += this.Battle.UI.GetRelativeTransformToParent(this.Battle.UI.GetParent()).origin;
			// position += button.GetRelativeTransformToParent(button.GetParent()).origin;
			// position += button.SoulPosition;

			// var position = new Vector2(0, 0);

			// this.Battle.Player.Position = position;

			var player = this.Battle.Player;
			player.GetParent().RemoveChild(player);
			button.AddChild(player);
			player.Position = button.SoulPosition;
		}

		private void _on_button_press(string name) {
			this.OnButtonPress?.Invoke(name);
		}

		public delegate void ButtonHandler(string name);
		public event ButtonHandler OnButtonPress;

	}
}
