namespace CreateYourDot.Scenes.Battle.UT.UI {
	using Godot;

	public class UIBox : Node2D
	{

		public Text.TextBox TextBox { get; private set; }

		public ColorRect Background { get; private set; }

		public ColorRect Border { get; private set; }

		[Export]
		private float _width = 575;

		[Export]
		private float _height = 140;

		[Export]
		private float _border_width = 5;

		public override void _Ready() {
			this.TextBox = this.GetNode<Text.TextBox>("TextBox");

			// The background and border are just rectangles.
			this.Background = this.GetNode<ColorRect>("Background");
			this.Border = this.GetNode<ColorRect>("Border");
			this._refresh();

		}

		public float Width {
			get => this._width;
			set {
				this._width = value;
				this._refresh();
			}
		}

		public float Height {
			get => this._height;
			set {
				this._height = value;
				this._refresh();
			}
		}

		public float BorderWidth {
			get => this._border_width;
			set {
				this._border_width = value;
				this._refresh();
			}
		}

		private void _refresh() {
			this.Border.RectSize = new Vector2(
				this._width,
				this._height
			);
			this.Background.RectSize = new Vector2(
				this._width - this._border_width * 2,
				this._height - this._border_width * 2
			);
			this.Background.RectPosition = new Vector2(
				this._border_width,
				this._border_width
			);

			this.TextBox.Position = new Vector2(
				this._border_width + 6,
				this._border_width + 2
			);

		}

	}
}
