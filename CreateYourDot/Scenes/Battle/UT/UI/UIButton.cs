namespace CreateYourDot.Scenes.Battle.UT.UI {
	using System;
	using Godot;

	public class UIButton : Node2D
	{

		[Export]
		public Texture UnactivatedTexture;

		[Export]
		public Texture ActivatedTexture;

		public Battle Battle { get; internal set; }

		private Sprite _sprite;

		public override void _Ready() {
			this._sprite = this.GetNode<Sprite>("Sprite");
			this._sprite.Texture = this.UnactivatedTexture;
		}

		public void Select() {
			this._sprite.Texture = this.ActivatedTexture;
		}

		public void Deselect() {
			this._sprite.Texture = this.UnactivatedTexture;
		}

		public void Press() {
			this.OnPress?.Invoke(this.Name);
			this._Press();
		}

		// 16 == soul size
		[Export]
		public Vector2 SoulPosition { get; set; } = new Vector2(16.0f, 15.0f + 16.0f / 2);

		protected virtual void _Press() { }

		public delegate void PressHandler(string name);

		public event PressHandler OnPress;

	}
}
