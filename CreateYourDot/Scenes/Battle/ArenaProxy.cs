namespace CreateYourDot.Scenes.Battle {
	using System;
	using Godot;

	public class ArenaProxy : StaticBody2D
	{

		public delegate void DrawerFunc(Node2D node);

		public DrawerFunc Drawer = (Node2D node) => {};

		public sealed override void _Draw() {
			this.Drawer(this);
		}
	}
}
