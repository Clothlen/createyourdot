namespace CreateYourDot.Scenes {
	using Godot;

	internal class CoreNode : Node
	{

		private static CoreNode _instance;

		public override void _EnterTree() {
			if (_instance != null) {
				throw new System.Exception("Already an instance of CoreNode running.");
			}
			_instance = this;
			GD.Print("CoreNode entering tree.");

			Core.init(this);
		}

		public static async System.Threading.Tasks.Task WaitForTimer(float time) {
			await _instance.ToSignal(_instance.GetTree().CreateTimer(time), "timeout");
		}

		public override void _UnhandledKeyInput(InputEventKey evt) {
			if (evt.IsActionPressed("quit")) {
				this.GetTree().Quit();
			}
		}
	}
}
