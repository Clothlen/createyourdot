namespace CreateYourDot.Scenes {
	using Godot;

	internal class FPSCounter : Node2D
	{

		private Label _label;

		public override void _Ready() {
			this._label = this.GetNode<Label>("Label");
		}

		public override void _PhysicsProcess(float delta) {
			this._label.Text = $"{Engine.GetFramesPerSecond()} FPS\n{Core.PHYSICS_TICKRATE / (delta * Core.PHYSICS_TICKRATE)} TPS";
		}

	}
}
