namespace CreateYourDot.Enemies {
	using System;
	using System.Collections.Generic;
	using Godot;

	public abstract class Enemy
	{

		public abstract string Id { get; }

		public Modding.Mod ParentMod { get; internal set; }

		public virtual string Name { get; set; } = "Spooky ghost enemy that wasn't given a name by their developer???";

		public Scenes.Battle.UT.Battle Battle { get; internal set; }

		public Node2D Node { get; internal set; }

		public virtual float MaxHealth { get; set; } = 100;

		public virtual float Health { get; set; } = 100;

		public virtual float Attack { get; set; } = 10;

		public virtual float Defense { get; set; } = 10;

		public virtual float Gold { get; set; } = 10_000;

		public virtual string CheckText { get; set; } = "The developer forgot to set their check text...";

		public virtual bool Unkillable { get; set; } = false;

		public virtual string[] Comments { get; set; } = new[] {
			"The developer forgot to set their comments."
		};

		public virtual string[] Commands { get; set; } = new string[0];

		public virtual string[] Dialogue { get; set; } = new[] { "Hi." };

		public virtual string DefenseMissText { get; set; } = "MISS";

		public virtual string NoAttackMissText { get; set; } = "MISS";

		public virtual bool CanSpare { get; set; } = false;

		public virtual bool CanCheck { get; set; } = true;

		public virtual string DialogueBubble { get; set; } = "";

		public virtual string Voice { get; set; } = "";

		internal void init(Scenes.Battle.UT.Battle battle) {
			this.Battle = battle;
			this.Create();
		}

		public virtual void Create() { }

		public virtual void Update(float delta) { }

		public void CreateSprite(string path) {
			// TODO: autocomplete path with mod id
			var tex = Loaders.ImageLoader.get_image_texture(path);

			var spr = new Sprite() {
				Texture = tex
			};
			this.Node.AddChild(spr);
		}

		public void CreateAsScene(string path) {

		}

		public void StartWave<T>()
		where T : Waves.Wave, new() {
			this.Battle.StartWave<T>(this.ParentMod);
		}

		public void StartWave(Type type) {
			this.Battle.StartWave(type, this.ParentMod);
		}

		public void StartRandomWave(IEnumerable<Type> waves) {
			this.Battle.StartRandomWave(waves, this.ParentMod);
		}

		public bool IsActive { get; set; } = true;

		// Like HandleAttack
		public virtual void OnAttacked(float attack_status) { GD.Print($"ATTACKED {attack_status} {this.Health}"); }

		// Called when it is the enemy's turn, after the player is done
		// doing things.
		public virtual void OnAttack() { }

		// Like HandleCustomCommand
		public virtual void OnCommand(string command) { }

		public virtual void OnDeath() { }

		public virtual void OnSpare() { }

		public virtual void BeforeDamageCalculation() { }

	}

}
