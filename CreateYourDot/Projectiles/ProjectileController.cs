namespace CreateYourDot.Projectiles {
	public abstract class ProjectileController : AbstractProjectileController
	{

		private Util.Signals.Area2DProxy.OnBodyEntered _on_body_entered_handler;
		private Util.Signals.Area2DProxy.OnBodyExited _on_body_exited_handler;

		public override void Destroy() {
			base.Destroy();

			this._on_body_entered_handler.Disconnect();
		}

		public void CreateAsSprite(string path) {
			this.CreateAsSprite(path, 0);
		}

		public void CreateAsSprite(string path, uint flags) {

			var image = new Godot.Image();
			image.Load(path);

			var texture = new Godot.ImageTexture();
			texture.CreateFromImage(image, flags);

			var colliders = Util.PhysicsUtil.ConvertSpriteToColliders(image);

			var sprite = new Godot.Sprite();
			sprite.Texture = texture;

			var texsize = texture.GetSize();

			var body = new Godot.Area2D();
			foreach (var b in colliders) {
				b.Position = new Godot.Vector2(-texsize.x / 2, -texsize.y / 2);
				body.AddChild(b);
			}

			this._on_body_entered_handler = new Util.Signals.Area2DProxy.OnBodyEntered(body, this._on_body_entered);
			this._on_body_exited_handler = new Util.Signals.Area2DProxy.OnBodyExited(body, this._on_body_exited);

			body.AddChild(sprite);

			this.Node = body;

		}

		public void CreateFromScene(Godot.PackedScene scene) {
			var instance = (Godot.Node2D)scene.Instance();
			this.Node = instance;
		}

		private void _on_body_entered(Godot.Node body) {
			this.OnBodyEntered((Godot.PhysicsBody2D)body);
		}

		private void _on_body_exited(Godot.Node body) {
			this.OnBodyExited((Godot.PhysicsBody2D)body);
		}

	}
}
