namespace CreateYourDot.Projectiles {
	public interface IDamager
	{
		void OnAttack(IDamageable thing);
	}
}
