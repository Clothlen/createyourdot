namespace CreateYourDot.Projectiles {
	public interface IDamageable
	{

		float Health { get; }

		void Damage(float amount);
	}
}
