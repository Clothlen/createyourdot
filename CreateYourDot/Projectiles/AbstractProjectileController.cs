namespace CreateYourDot.Projectiles {
	using System.Collections.Generic;

	public abstract class AbstractProjectileController : IProjectileController
	{

		public Scenes.Battle.AbstractBattle Battle { get; internal set; }

		public Arena.IArena Arena { get; internal set; }

		public Godot.Node2D Node { get; protected set; }

		public Godot.Node2D Parent { get; protected set; }

		public virtual bool Persistant { get; protected set; }

		protected readonly List<IDamageable> _Damageables = new List<IDamageable>();

		public virtual void Create() {
			this.Parent = this.Arena.Node;
		}

		public virtual void Update(float delta) {
			foreach (var target in this._Damageables) {
				this.OnHit(target);
			}
		}

		public virtual void Destroy() { }

		public virtual void OnBodyEntered(Godot.PhysicsBody2D body) {
			if (body is IDamageable target) {
				this._Damageables.Add(target);
			}
		}

		public virtual void OnBodyExited(Godot.PhysicsBody2D body) {
			if (body is IDamageable target) {
				// For them to exit the body, they must have previously entered it.
				// TODO: Possibly unless they were spawned inside?
				this._Damageables.Remove(target);
			}
		}

		public virtual void OnHit(IDamageable target) { }

		public virtual float x {
			get {
				return this.Node.GlobalPosition.x - this.Parent.GlobalPosition.x;
			}
			set {
				this.Node.GlobalPosition = new Godot.Vector2(value + this.Parent.GlobalPosition.x, this.Node.GlobalPosition.y);
			}
		}

		public virtual float y {
			get {
				// return this.Node.GlobalPosition.y - this.Parent.GlobalPosition.y;
				return -(this.Node.GlobalPosition.y - this.Parent.GlobalPosition.y);
			}
			set {
				// this.Node.GlobalPosition = new Godot.Vector2(this.Node.GlobalPosition.x, value + this.Parent.GlobalPosition.y);
				this.Node.GlobalPosition = new Godot.Vector2(this.Node.GlobalPosition.x, this.Parent.GlobalPosition.y - value);
			}
		}

		public virtual Godot.Vector2 Position {
			get {
				return new Godot.Vector2(
					this.Node.GlobalPosition.x - this.Parent.GlobalPosition.x,

					// this.Node.GlobalPosition.y - this.Parent.GlobalPosition.y
					-(this.Node.GlobalPosition.y - this.Parent.GlobalPosition.y)
				);
			}
			set {
				this.Node.GlobalPosition = this.Parent.GlobalPosition + value;
			}
		}

		public virtual void Move(float x, float y) {
			this.x += x;
			this.y += y;
		}

		public virtual void MoveTo(float x, float y) {
			this.x = x;
			this.y = y;
		}

		public virtual void MoveTo(Godot.Vector2 value) {
			this.Position = value;
		}

	}
}
