namespace CreateYourDot.Projectiles {
	public interface IProjectileController : Util.INodeController<Godot.Node2D>
	{

		/// <summary>
		/// A reference to the current parent battle.
		/// </summary>
		/// <value></value>
		Scenes.Battle.AbstractBattle Battle { get; }

		/// <summary>
		/// A reference to the Arena being used in this battle.
		/// </summary>
		/// <value>The Arena being used.</value>
		Arena.IArena Arena { get; }

		/// <summary>
		/// A reference not to the parent of the node, but of the anchor.
		/// </summary>
		/// <value></value>
		Godot.Node2D Parent { get; }

		/// <summary>
		/// When a wave ends, it will destroy all projectiles that it spawned.
		///
		/// If this is set to true, it will be spared.
		/// </summary>
		/// <value></value>
		bool Persistant { get; }

		/// <summary>
		/// Should be called when the projectile is being spawned.
		/// </summary>
		void Create();

		/// <summary>
		/// Should be called every tick that the projectile is being ticked.
		/// </summary>
		/// <param name="delta">The time since the last call.</param>
		void Update(float delta);

		/// <summary>
		/// Should be called when the projectile should be destroyed.
		///
		/// This allows the projectile to do teardown / whatever it wants to
		/// do.
		/// </summary>
		void Destroy();

		/// <summary>
		/// Should be called when the projectile body makes contact with another body.
		/// </summary>
		/// <param name="body"></param>
		void OnBodyEntered(Godot.PhysicsBody2D body);

		/// <summary>
		/// Should be called when the projectile hits something
		///
		/// The projectile can deal damage, but this is optional.
		/// </summary>
		/// <param name="target">The thing to attack.</param>
		void OnHit(IDamageable target);

		/// <summary>
		/// The x position of the projectile.
		///
		/// This is relative to the Arena's center.
		/// </summary>
		/// <value></value>
		float x { get; set; }

		// /// <summary>
		// /// The y position of the projectile.
		// ///
		// /// This is relative to the Arena's center.
		// /// </summary>
		// /// <value></value>
		// float y { get; set; }

		// Godot.Vector2 Position { get; set; }

		// /// <summary>
		// /// The "real" x position of the projectile.
		// /// </summary>
		// /// <value></value>
		// float Absx { get; set; }

		// /// <summary>
		// /// The "real" y position of the projectile.
		// /// </summary>
		// /// <value></value>
		// float Absy { get; set; }

		// Godot.Vector2 AbsolutePosition { get; set; }

		/// <summary>
		/// Move x units to the right and y units downwards.
		///
		/// NOTICE: The y is opposite to CreateYourFrisk.
		///
		/// Negative x moves it to the left, and negative y moves it upward.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		void Move(float x, float y);

		// /// <summary>
		// /// As `Move(float, float)`, but with a <see cref="Godot.Vector2"/>.
		// /// </summary>
		// /// <param name="value"></param>
		// void Move(Godot.Vector2 value);

		// /// <summary>
		// /// Move immediately.
		// /// </summary>
		// /// <param name="x"></param>
		// /// <param name="y"></param>
		// void MoveTo(float x, float y);

		// void MoveTo(Godot.Vector2 value);

		// /// <summary>
		// /// Move immediately to an absolute position.
		// /// </summary>
		// /// <param name="x"></param>
		// /// <param name="y"></param>
		// void MoveToAbs(float x, float y);

		// void MoveToAbs(Godot.Vector2 value);

		// // void Remove();

		// string this[string name] { get; set; }

		// string GetVar(string name);

		// void SetVar(string name, object value);

		// // void SendToTop();

		// // void SendToBottom();

		// // bool isColliding();
		// // bool Active { get; set; }

	}
}
